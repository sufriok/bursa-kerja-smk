<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Study;
use App\Business;
use App\Work;

class Income extends Model
{
    protected $fillable = ['nominal'];

    public function study()
    {
        return $this->hasMany(Study::class, 'income_id', 'id');
    }

    public function business()
    {
        return $this->hasMany(Business::class, 'income_id', 'id');
    }

    public function work()
    {
        return $this->hasMany(Work::class, 'income_id', 'id');
    }
}
