<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\User;

class Graduate extends Authenticatable 
{
    protected $fillable = [
        'name',
        'nisn',
        'th_lulus',
        'keterangan',
        'department',
        'user_id',
    ];
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}
