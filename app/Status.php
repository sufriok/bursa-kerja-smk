<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Study;
use App\Business;
use App\Work;

class Status extends Model
{
    protected $fillable = ['status'];

    public function study()
    {
        return $this->hasMany(Study::class, 'status_id', 'id');
    }

    public function business()
    {
        return $this->hasMany(Business::class, 'status_id', 'id');
    }

    public function work()
    {
        return $this->hasMany(Work::class, 'status_id', 'id');
    }
}
