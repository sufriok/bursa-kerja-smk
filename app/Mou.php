<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mou extends Model
{
    protected $fillable = [
        'perusahaan',
        'logo',
        ];
}
