<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use App\Category;

class Loker extends Model
{
    protected $fillable = [
        'title',
        'deskripsi',
        'perusahaan',
        'tgl_awal',
        'tgl_akhir',
        'brosur'
        ];

    public function getTglAwalAttribute()
    {
        return Carbon::parse($this->attributes['tgl_awal'])
        ->translatedFormat('d F Y');
    }

    public function getTglAkhirAttribute()
    {
        return Carbon::parse($this->attributes['tgl_akhir'])
        ->translatedFormat('d F Y');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
