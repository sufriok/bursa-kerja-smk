<?php

namespace App\Exports;

use App\Graduate;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class GraduateExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function view(): View
    {
        return view('backend.exports.invoices', [
            'graduates' => Graduate::all()
        ]);
    }
}
