<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Location;
use App\Income;
use App\Status;

class Work extends Model
{
    protected $fillable = ['status_id','location_id', 'income_id'];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function income()
    {
        return $this->belongsTo(Income::class);
    }
}
