<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Loker;

class Category extends Model
{
    protected $fillable = ['kategori'];

    public function loker()
    {
        return $this->hasMany(Loker::class, 'category_id', 'id');
    }
}
