<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Website;

use File;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $website = Website::first();
        $users = User::where('status', "1")->get();
        return view('backend.admin.index', compact('website', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->status = "1";
            $user->save(); 

        return redirect()->route('admin.index')->with('success', 'Data telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $website = Website::first();
        $user = User::find($id);
        return view('backend.admin.edit', compact('website', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        if($request->profil == null)
        {

            if($request->password == null)
            {
                $user = User::find($id);
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = $user->password; 
                $user->push(); 
            }else
            {
                $user = User::find($id);
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->push(); 
            }

        }else
        {
    
            $destination = "public/profil";

            $profil = $request->file('profil');
            $extension = $profil->getClientOriginalExtension(); 
            // RENAME THE UPLOAD WITH RANDOM NUMBER 
            $profilName = rand(11111, 33333) . '.' . $extension; 
            // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
            $profil->move($destination, $profilName);

            // hapus file
            // $gambar = User::where('id',$id)->first();
            // File::delete('public/profil/'.$gambar->profil);

            if($request->password == null)
            {
                $user = User::find($id);
                $user->name = $request->name;
                $user->email = $request->email;
                $user->profil = $profilName;
                $user->password = $user->password; 
                $user->push(); 
            }else
            {
                $user = User::find($id);
                $user->name = $request->name;
                $user->email = $request->email;
                $user->profil = $profilName;
                $user->password = Hash::make($request->password);
                $user->push(); 
            } 
        }

        return redirect()->route('admin.index')->with('update', 'Data telah diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('admin.index')->with('delete', 'Data telah dihapus');
    }
}
