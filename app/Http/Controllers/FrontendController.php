<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Website;
use App\Loker;
use App\Category;
use App\Message;
use App\Mou;
use App\Graduate;
use App\Study;
use App\Business;
use App\Work;
use App\Income;
use App\Status;
use App\Location;
use App\User;
use File;

class FrontendController extends Controller
{
    public function beranda()
    {
        $website = Website::first();

        $jmlhloker = Loker::count();
        $jmlhgraduate = Graduate::count();
        $jmlhmou = Mou::count();

        if ($jmlhloker >= 6) {
            $lokers = Loker::orderBy('id','DESC')->paginate(6);
        }else {
            $lokers = Loker::orderBy('id','DESC')->paginate(3);
        }
        
        $mous = Mou::all();
        $categories = Category::all();
        return view('frontend.beranda', compact('website', 'lokers', 'mous', 'jmlhloker', 'jmlhgraduate', 'jmlhmou', 'categories'));
    }

    public function showAllLoker()
    {
        $website = Website::first();
        $lokers = Loker::orderBy('id','DESC')->paginate(3);
        $newlokers = Loker::orderBy('id','DESC')->paginate(5);
        $categories = Category::all();
        return view('frontend.loker_all', compact('website', 'lokers', 'newlokers', 'categories'));
    }

    public function cariLoker(Request $request)
	{
		$website = Website::first();

        // menangkap data pencarian
		$cari = $request->cari;
 
    		// mengambil data dari table pegawai sesuai pencarian data
		$lokers = Loker::where('title','like',"%".$cari."%")
		->paginate(3);
        $newlokers = Loker::orderBy('id','DESC')->paginate(5);
        $categories = Category::all();
 
    		// mengirim data pegawai ke view index
		return view('frontend.loker_all', compact('website', 'lokers', 'newlokers', 'categories'));
 
	}

    public function showLoker($id)
    {
        $website = Website::first();
        $categories = Category::all();
        $loker = Loker::find($id);
        return view('frontend.show_loker', compact('website', 'loker', 'categories'));
    }

    public function categoryLoker($id)
    {
        $website = Website::first();
        
        $newlokers = Loker::orderBy('id','DESC')->paginate(5);
        $categories = Category::all();
        $lokers = Loker::where('category_id',$id)
        ->orderBy('id','DESC')->paginate(3);
        return view('frontend.loker_all', compact('website', 'lokers', 'newlokers', 'categories'));
    }

    public function kirimMessage(Request $request)
    {
        $message = new Message;
            $message->name = $request->name;
            $message->email = $request->email;
            $message->subject = $request->subject;
            $message->pesan = $request->pesan;
            $message->save(); 

        return redirect('/home')->with('success', 'Pesan anda telah terkirim!');
    }

    public function registerGraduate(){
        
        $website = Website::first();
        $id = Auth::id();
        $statuses = Status::all();
        $incomes = Income::all();
        $locations = Location::all();
        $categories = Category::all();
        $graduate = Graduate::where('id', $id)->first();
        $study = Study::where('graduate_id', $id)->first();
        $business = Business::where('graduate_id', $id)->first();
        $work = Work::where('graduate_id', $id)->first();
        return view('frontend.auth.registerGraduate', compact('website','graduate', 'study', 'business', 'work', 'statuses', 'incomes', 'locations', 'categories'));
        
    }

    public function pilihGraduate($id)
    {
        $iduser = Auth::id();
        $website = Website::first();
        $categories = Category::all();
        $alumni = Graduate::find($id);
            $alumni->user_id = $iduser; 
            $alumni->push();
        $graduate = Graduate::where('user_id', $iduser)->first();
        return back();
    }

    //status kuliah
    public function ketKuliah($id)
    {
        $website = Website::first();
        $categories = Category::all();
        $graduate = Graduate::find($id);
        $statuses = Status::all();
        return view('frontend.keterangan.ketKuliah', compact('website', 'categories', 'graduate', 'statuses'));
    }

    public function profilKuliah(Request $request, $id)
    {
        
        // dd($request->all());
        $graduate = Graduate::find($id);
        // dd($graduate->id);
        $business = Business::where('graduate_id', $graduate->id)->get();
        $work = Work::where('graduate_id', $graduate->id)->get();

        if (!$business->isEmpty()) {

            $graduate->keterangan = "kuliah";
            $graduate->push();
            $id_alumni = $graduate->id;
            $study = new Study;
                $study->nma_prgu = $request->nma_prgu;
                $study->jurusan = $request->jurusan;
                $study->mulai = $request->mulai;
                $study->status_id = $request->status_id;
                $study->graduate_id = $id_alumni;
                $study->save(); 

                $business = Business::where('graduate_id', $graduate->id)->first();
                $business->delete();

            return redirect('/home')->with('success', 'Status wirausaha dihapus dan status kuliah telah ditambahkan');

        }elseif (!$work->isEmpty()) {
             
            $graduate->keterangan = "kuliah";
            $graduate->push();
            $id_alumni = $graduate->id;
            $study = new Study;
                $study->nma_prgu = $request->nma_prgu;
                $study->jurusan = $request->jurusan;
                $study->mulai = $request->mulai;
                $study->status_id = $request->status_id;
                $study->graduate_id = $id_alumni;
                $study->save(); 

                $work = work::where('graduate_id', $graduate->id)->first();
                $work->delete();
            
            return redirect('/home')->with('success', 'Status bekerja dihapus dan status kuliah telah ditambahkan');

        }else {
            $graduate->keterangan = "kuliah";
            $graduate->push();
            $id_alumni = $graduate->id;
            $study = new Study;
                $study->nma_prgu = $request->nma_prgu;
                $study->jurusan = $request->jurusan;
                $study->mulai = $request->mulai;
                $study->status_id = $request->status_id;
                $study->graduate_id = $id_alumni;
                $study->save(); 

            return redirect('/home')->with('success', 'Status kuliah telah ditambahkan');
        }
        
    }

    public function updateProfilKuliah(Request $request, $id)
    {
        // dd($request->all());
        $study = Study::find($id);
        $study->nma_prgu = $request->nma_prgu;
        $study->jurusan = $request->jurusan;
        $study->mulai = $request->mulai;
        $study->status_id = $request->status_id;
        $study->push(); 

        return redirect('/home')->with('success', 'Status kuliah telah diperbaharui');
    }

    //status wirausaha
    public function ketWirausaha($id)
    {
        $website = Website::first();
        $categories = Category::all();
        $graduate = Graduate::find($id);
        $incomes = Income::all();
        $statuses = Status::all();
        return view('frontend.keterangan.ketWirausaha', compact('website', 'categories', 'graduate', 'statuses', 'incomes'));
    }

    public function profilWirausaha(Request $request, $id)
    {
        
        // dd($request->all());
        $graduate = Graduate::find($id);
        $study = Study::where('graduate_id', $graduate->id)->get();
        $work = Work::where('graduate_id', $graduate->id)->get();
        if (!$study->isEmpty()) {
            
            $graduate->keterangan = "wirausaha";
            $graduate->push();
            $id_alumni = $graduate->id;
            $business = new Business;
                $business->nma_pru = $request->nma_pru;
                $business->jenis = $request->jenis;
                $business->mulai = $request->mulai;
                $business->status_id = $request->status_id;
                $business->income_id = $request->income_id;
                $business->graduate_id = $id_alumni;
                $business->save(); 

                $study = Study::where('graduate_id', $graduate->id)->first();
                $study->delete();
    
            return redirect('/home')->with('success', 'Status kuliah dihapus dan status wirausaha telah ditambahkan');

        }elseif (!$work->isEmpty()) {
             
            $graduate->keterangan = "wirausaha";
            $graduate->push();
            $id_alumni = $graduate->id;
            $business = new Business;
                $business->nma_pru = $request->nma_pru;
                $business->jenis = $request->jenis;
                $business->mulai = $request->mulai;
                $business->status_id = $request->status_id;
                $business->income_id = $request->income_id;
                $business->graduate_id = $id_alumni;
                $business->save(); 

                $work = Work::where('graduate_id', $graduate->id)->first();
                $work->delete();
    
            return redirect('/home')->with('success', 'Status bekerja dihapus dan status wirausaha telah ditambahkan');

        }else {

            $graduate->keterangan = "wirausaha";
            $graduate->push();
            $id_alumni = $graduate->id;
            $business = new Business;
                $business->nma_pru = $request->nma_pru;
                $business->jenis = $request->jenis;
                $business->mulai = $request->mulai;
                $business->status_id = $request->status_id;
                $business->income_id = $request->income_id;
                $business->graduate_id = $id_alumni;
                $business->save(); 
    
            return redirect('/home')->with('success', 'Status wirausaha telah ditambahkan');
        }

    }

    public function updateProfilWirausaha(Request $request, $id)
    {
        // dd($request->all());
        $business = Business::find($id);
        $business->nma_pru = $request->nma_pru;
        $business->jenis = $request->jenis;
        $business->mulai = $request->mulai;
        $business->status_id = $request->status_id;
        $business->income_id = $request->income_id;
        $business->push(); 

        return redirect('/home')->with('success', 'Status wirausaha telah diperbaharui');
    }

    //status bekerja
    public function ketBekerja($id)
    {
        $website = Website::first();
        $categories = Category::all();
        $graduate = Graduate::find($id);
        $incomes = Income::all();
        $statuses = Status::all();
        $locations = Location::all();
        return view('frontend.keterangan.ketBekerja', compact('website', 'categories', 'graduate', 'incomes', 'statuses', 'locations'));
    }

    public function profilBekerja(Request $request, $id)
    {
        // dd($request->all());
        $graduate = Graduate::find($id);
        $study = Study::where('graduate_id', $graduate->id)->get();
        $business = Business::where('graduate_id', $graduate->id)->get();

        if (!$study->isEmpty()) {
            
            $graduate->keterangan = "bekerja";
            $graduate->push();
            $id_alumni = $graduate->id;
            $work = new Work;
            $work->nma_pru = $request->nma_pru;
            $work->posisi = $request->posisi;
            $work->mulai = $request->mulai;
            $work->status_id = $request->status_id;
            $work->sesu_bidkeah = $request->sesu_bidkeah;
            $work->perjanjian = $request->perjanjian;
            $work->bidus = $request->bidus;
            $work->location = $request->location;
            $work->income_id = $request->income_id;
            $work->graduate_id = $id_alumni;
            $work->save(); 

                $study = Study::where('graduate_id', $graduate->id)->first();
                $study->delete();
    
            return redirect('/home')->with('success', 'Status kuliah dihapus dan status bekerja telah ditambahkan');

        }elseif (!$business->isEmpty()) {

            $graduate->keterangan = "bekerja";
            $graduate->push();
            $id_alumni = $graduate->id;
            $work = new Work;
            $work->nma_pru = $request->nma_pru;
            $work->posisi = $request->posisi;
            $work->mulai = $request->mulai;
            $work->status_id = $request->status_id;
            $work->sesu_bidkeah = $request->sesu_bidkeah;
            $work->perjanjian = $request->perjanjian;
            $work->bidus = $request->bidus;
            $work->location = $request->location;
            $work->income_id = $request->income_id;
            $work->graduate_id = $id_alumni;
            $work->save(); 

                $business = Business::where('graduate_id', $graduate->id)->first();
                $business->delete();
    
            return redirect('/home')->with('success', 'Status wirausaha dihapus dan status bekerja telah ditambahkan');

        }else {
             
            $graduate->keterangan = "bekerja";
            $graduate->push();
            $id_alumni = $graduate->id;
            $work = new Work;
            $work->nma_pru = $request->nma_pru;
            $work->posisi = $request->posisi;
            $work->mulai = $request->mulai;
            $work->status_id = $request->status_id;
            $work->sesu_bidkeah = $request->sesu_bidkeah;
            $work->perjanjian = $request->perjanjian;
            $work->bidus = $request->bidus;
            $work->location = $request->location;
            $work->income_id = $request->income_id;
            $work->graduate_id = $id_alumni;
            $work->save();
    
            return redirect('/home')->with('success', 'Status bekerja telah ditambahkan');

        }
        

    }

    public function updateProfilBekerja(Request $request, $id)
    {
        // dd($request->all());
        $work = Work::find($id);
        $work->nma_pru = $request->nma_pru;
        $work->posisi = $request->posisi;
        $work->mulai = $request->mulai;
        $work->status_id = $request->status_id;
        $work->sesu_bidkeah = $request->sesu_bidkeah;
        $work->perjanjian = $request->perjanjian;
        $work->bidus = $request->bidus;
        $work->location = $request->location;
        $work->income_id = $request->income_id;
        $work->push(); 

        return redirect('/home')->with('success', 'Status bekerja telah diperbaharui');
    }

    public function updateProfil(Request $request, $id)
    {
        
        if($request->profil == null)
        {

            if($request->password == null)
            {
                $user = User::find($id);
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = $user->password; 
                $user->push(); 
            }else
            {
                $user = User::find($id);
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->push(); 
            }

        }else
        {
    
            $destination = "public/profil";

            $profil = $request->file('profil');
            $extension = $profil->getClientOriginalExtension(); 
            // RENAME THE UPLOAD WITH RANDOM NUMBER 
            $profilName = rand(44444, 66666) . '.' . $extension; 
            // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
            $profil->move($destination, $profilName);

            // hapus file
            // $gambar = User::where('id',$id)->first();
            // File::delete('public/profil/'.$gambar->profil);

            if($request->password == null)
            {
                $user = User::find($id);
                $user->name = $request->name;
                $user->email = $request->email;
                $user->profil = $profilName;
                $user->password = $user->password; 
                $user->push(); 
            }else
            {
                $user = User::find($id);
                $user->name = $request->name;
                $user->email = $request->email;
                $user->profil = $profilName;
                $user->password = Hash::make($request->password);
                $user->push(); 
            } 
        }

        return redirect('/home')->with('update', 'Data telah diperbarui');
    }

    
}
