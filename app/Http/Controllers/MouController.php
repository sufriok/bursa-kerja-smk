<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mou;
use App\Website;
use File;

class MouController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $website = Website::first();
        $mous = Mou::all();
        return view('backend.mou.index', compact('website', 'mous'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $destination = "public/logoMou";

        $logo = $request->file('logo');
        $extension = $logo->getClientOriginalExtension(); 
        // RENAME THE UPLOAD WITH RANDOM NUMBER 
        $logoName = rand(11111, 33333) . '.' . $extension; 
        // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
        $logo->move($destination, $logoName);

        $mou = new Mou;
        //$absent->tgl_wak = $tgl_wak;
        $mou->perusahaan = $request->perusahaan;
        $mou->logo = $logoName;
        $mou->save(); 

        return redirect()->route('mou.index')->with('success', 'Data telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $website = Website::first();
        $mou = Mou::find($id);
        return view('backend.mou.edit', compact('website', 'mou'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->logo == null)
        {

            //$idUser = Auth::user()->id;
            //$tgl_wak = Carbon::now()->toDateTimeString('d-M-Y H:i');
            


            $mou = Mou::find($id);
            //$absent->tgl_wak = $tgl_wak;
            $mou->perusahaan = $request->perusahaan;
            // $absent->foto = $absent->foto;
            // $absent->user_id = $idUser;
            $mou->push();

        }else
        {
            //$idUser = Auth::user()->id;
            //$tgl_wak = Carbon::now()->toDateTimeString('d-M-Y H:i');
            

            $destination = "public/logoMou";

            $logo = $request->file('logo');
            $extension = $logo->getClientOriginalExtension(); 
            // RENAME THE UPLOAD WITH RANDOM NUMBER 
            $logoName = rand(11111, 33333) . '.' . $extension; 
            // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
            $logo->move($destination, $logoName);

            // hapus file
            $gambar = Mou::where('id',$id)->first();
            File::delete('public/logoMou/'.$gambar->logo);

            $mou = Mou::find($id);
            //$absent->tgl_wak = $tgl_wak;
            $mou->perusahaan = $request->perusahaan;
            $mou->logo = $logoName;
            // $absent->user_id = $idUser;
            $mou->push(); 
        }

        return redirect()->route('mou.index')->with('update', 'Data telah diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // hapus file
		$mou = Mou::where('id',$id)->first();
		File::delete('public/logoMou/'.$mou->logo);
 
		// hapus data
        Mou::destroy($id);

        return redirect()->route('mou.index')->with('delete', 'Data telah dihapus');
    }
}
