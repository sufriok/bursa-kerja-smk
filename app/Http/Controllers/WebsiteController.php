<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Website;
use File;

class WebsiteController extends Controller
{
    public function index()
    {
        // $graduates = Graduate::all();
        // $studies = Study::all();
        // $business = Business::all(); 
        
        $website = Website::first();
        // dd($ket, $cekstudy, $cekgraduate);
        
        return view('backend.manajemen_website.index', compact('website'));
    }

    public function update(Request $request)
    {
        if(isset($request->logo))
        {

            $destination = "public/images";

            $logo = $request->file('logo');
            $extension = $logo->getClientOriginalExtension(); 
            // RENAME THE UPLOAD WITH RANDOM NUMBER 
            $logoName = rand(11111, 33333) . '.' . $extension; 
            // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
            $logo->move($destination, $logoName);

            // hapus file
            $gambar = Website::first();
            File::delete('public/images/'.$gambar->header_logo);

            $website = Website::first();
            $website->header_logo = $logoName;
            $website->push(); 

        }elseif(isset($request->about_foto))
        {
            $destination = "public/images";

            $about_foto = $request->file('about_foto');
            $extension = $about_foto->getClientOriginalExtension(); 
            // RENAME THE UPLOAD WITH RANDOM NUMBER 
            $fotoName = rand(11111, 33333) . '.' . $extension; 
            // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
            $about_foto->move($destination, $fotoName);

            // hapus file
            $gambar = Website::first();
            File::delete('public/images/'.$gambar->about_foto);

            $website = Website::first();
            $website->about_foto = $fotoName;
            $website->push(); 

        }else
        {
            $website = Website::first();
            $website->header_title = $request->header_title;
            $website->header_kalimat1 = $request->header_kalimat1;
            $website->header_kalimat2 = $request->header_kalimat2;
            $website->about_title = $request->about_title;
            $website->about_deskripsi = $request->about_deskripsi;
            $website->contact_alamat = $request->contact_alamat;
            $website->contact_email = $request->contact_email;
            $website->contact_phone = $request->contact_phone;
            $website->footer_twitter = $request->footer_twitter;
            $website->footer_facebook = $request->footer_facebook;
            $website->footer_instagram = $request->footer_instagram;
            $website->footer_youtube = $request->footer_youtube;
            $website->push();
            
        }

        return redirect()->route('manajemen.website')->with('update', 'Data telah diperbarui');
    }

    
}
