<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Mou;
use App\Loker;
use App\Website;
use App\Category;
use App\Status;
use App\Income; 
use App\Location;
use App\Graduate;
use App\Study;
use App\Business;
use App\Work;
use App\Post;
use App\Message;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        if(auth()->check() && Auth::user()->status == '1'){

            $website = Website::first();
            //$user = Auth::user()->id;
            //return view('admin.dashboard', compact('user'));
            $countgraduate = Graduate::count();
            $countloker = Loker::count();
            $countmou = Mou::count();
            $countuser = User::where('status', "2")->count();
            $users = User::where('status', "2")->get();
            $messages = Message::all();
            $posts = Post::orderBy('id','DESC')->paginate(6);

            return view('backend.dashboard', compact('website', 'countgraduate', 'countloker', 'countmou', 'countuser', 'users', 'messages', 'posts'));

        }elseif(auth()->check() && Auth::user()->status == '2'){

            $website = Website::first();
            $id = Auth::id();
            $posts = Post::where('user_id', $id)
                ->orderBy('id', 'DESC')->get();
            $statuses = Status::all();
            $incomes = Income::all();
            $categories = Category::all();

            $alumni = Graduate::all();
            foreach ($alumni as $graduate)
                {
                    $userid[] = $graduate->user_id;
                    $usersid = array_values($userid);
                }

            if (in_array($id,$usersid)) {
                $graduate = Graduate::where('user_id', $id)->first();
                $study = Study::where('graduate_id', $graduate->id)->first();
                $business = Business::where('graduate_id', $graduate->id)->first();
                $work = Work::where('graduate_id', $graduate->id)->first();
                return view('frontend.auth.profil', compact('website', 'posts','graduate', 'study', 'business', 'work', 'statuses', 'incomes', 'categories'));
            }else {
                $graduates = Graduate::whereNull('user_id')->get();
                return view('frontend.auth.registerGraduate', compact('website', 'posts', 'statuses', 'incomes', 'graduates', 'categories'));
            }
        
        }else{
                
            return view('auth.login');
        }
    }

    public function destroyPesan($id)
    {
    $message = Message::find($id);
    $message->delete();

    return redirect()->back()->with('delete', 'Pesan telah dihapus');
    }

    public function destroyPost($id)
    {
    $post = Post::find($id);
    $post->delete();

    return redirect()->back()->with('delete', 'Postingan telah dihapus');
    }

}
