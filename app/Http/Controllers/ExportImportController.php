<?php

namespace App\Http\Controllers;

use Session;
use File;

use Illuminate\Http\Request;
use App\Exports\GraduateExport;
use App\Imports\GraduateImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class ExportImportController extends Controller
{
    public function export_excel()
	{
		return Excel::download(new GraduateExport, 'data_alumni.xlsx');
	}

    public function import_excel(Request $request) 
	{
		// validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
 
		// menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file_siswa di dalam folder public
		$file->move('public/file_alumni',$nama_file);
 
		// import data
		Excel::import(new GraduateImport, public_path('/file_alumni/'.$nama_file));
 
		// notifikasi dengan session
		// Session::flash('sukses','Data Alumni Berhasil Diimport!');

		File::delete('public/file_alumni/'.$nama_file);
 
		// alihkan halaman kembali
		return redirect('/manajemen_alumni')->with('success', 'Data Alumni Berhasil Diimport!');
	}
}
