<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Loker;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category;
        $category->kategori = $request->kategori;
        $category->save(); 

        return redirect()->route('bursa_kerja.index')->with('success', 'Kategori baru telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) 
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($id);
        $category = Category::find($id);;
            $category->kategori = $request->kategori;
            $category->push();
        // $values=array('column1'=>'value','column2'=>'value2');
        // ItemTable::whereIn('id','value')->update($values);

        return redirect()->route('bursa_kerja.index')->with('warning', 'Kategori telah diupdate');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // hapus file
		// $loker = Loker::where('id',$id)->first();
		// File::delete('brosurKerja/'.$loker->brosur);
        $lokers = Loker::all();
        foreach ($lokers as $loker)
                {
                    $cid[] = $loker->category_id;
                    $cids = array_values($cid);
                }

            if(in_array($id,$cids)) {

                return redirect()->route('bursa_kerja.index')->with('warning', 'Tidak dapat menghapus kategori yang memiliki bursa kerja (loker)');
            
            }else {
                
                $category = Category::find($id);
                $category->delete();

                return redirect()->route('bursa_kerja.index')->with('delete', 'Kategori telah dihapus');
                
            }
        
    }
}
