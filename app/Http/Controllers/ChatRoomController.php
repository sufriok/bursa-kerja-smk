<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use App\Website;
use App\Category;
use Auth;
use App\Graduate;
use File;

class ChatRoomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $graduate = Graduate::where('user_id', $id)->first();
        $posts = Post::orderBy('id', 'DESC')->get();
        $comments = Comment::all();
        $website = Website::first();
        $categories = Category::all();
        return view('frontend.chatRoom.index', compact('graduate', 'posts', 'comments', 'website', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Auth::id();
        
        if($request->image == null){

            $post = new Post;
            $post->content = $request->content;
            $post->user_id = $id;
            $post->save();
        }else{

            $destination = "public/postImage";

            $image = $request->file('image');
            $extension = $image->getClientOriginalExtension(); 
            // RENAME THE UPLOAD WITH RANDOM NUMBER 
            $imageName = rand(11111, 33333) . '.' . $extension; 
            // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
            $image->move($destination, $imageName);
            
            $post = new Post;
            $post->content = $request->content;
            $post->image = $imageName;
            $post->user_id = $id;
            $post->save();
        }
         

        return redirect()->route('chatroom.index')->with('success', 'Data telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $website = Website::first();
        $categories = Category::all();
        return view('frontend.chatRoom.show', compact('post', 'website', 'categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if($post->image != null){
            File::delete('public/postImage'.$post->image);
        }else{
            
        }
        $post->delete();

        return redirect()->back()->with('delete', 'Postingan telah dihapus');
    }
}
