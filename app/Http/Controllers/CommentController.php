<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Broadcasting\SendComment;
use App\Post;
use Auth;
use App\Comment;

class CommentController extends Controller
{
    public function index(Post $post)
    {
        $comments = $post->comments()->with('user')->orderBy('id', 'desc')->get();

        return $comments;
    }
    
    public function store(Request $request, Post $post)
    {
        // $post = Post::find($id);
        // $idpost = $post->id;
        // $iduser = Auth::id();

        // $comment = New Comment;
        // $comment->content = $request->content;
        // $comment->user_id = $iduser;
        // $comment->post_id = $idpost;
        // $comment->save();

        // return redirect()->back()->with('success', 'your message,here');
        
        $comment = New Comment;
        $comment->user_id = auth()->user()->id;
        $comment->content = $request->content;

        $post->comments()->save($comment);

        $comment = Comment::where('id', $comment->id)->with('user')->first();

        broadcast(new SendComment($comment))->toOthers();

        return $comment;
    }
}
