<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Graduate;
use App\Study;
use App\Business;
use App\Work;
use App\Income;
use App\Status;
use App\Location;
use App\Website;

class KeteranganController extends Controller
{
    //status kuliah
    public function pilihKuliah($id)
    {
        $website = Website::first();
        $graduate = Graduate::find($id);
        $statuses = Status::all();
        return view('backend.manajemen_alumni.kuliah', compact('website', 'graduate', 'statuses'));
    }

    public function kuliah(Request $request, $id)
    {
        
        // dd($request->all());
        $graduate = Graduate::find($id);
        // dd($graduate->id);
        $business = Business::where('graduate_id', $graduate->id)->get();
        $work = Work::where('graduate_id', $graduate->id)->get();

        if (!$business->isEmpty()) {

            $graduate->keterangan = "kuliah";
            $graduate->push();
            $id_alumni = $graduate->id;
            $study = new Study;
                $study->nma_prgu = $request->nma_prgu;
                $study->jurusan = $request->jurusan;
                $study->mulai = $request->mulai;
                $study->status_id = $request->status_id;
                $study->graduate_id = $id_alumni;
                $study->save(); 

                $business = Business::where('graduate_id', $graduate->id)->first();
                $business->delete();

            return redirect()->route('manajemen_alumni.index')->with('success', 'Status wirausaha dihapus dan status kuliah telah ditambahkan');

        }elseif (!$work->isEmpty()) {
             
            $graduate->keterangan = "kuliah";
            $graduate->push();
            $id_alumni = $graduate->id;
            $study = new Study;
                $study->nma_prgu = $request->nma_prgu;
                $study->jurusan = $request->jurusan;
                $study->mulai = $request->mulai;
                $study->status_id = $request->status_id;
                $study->graduate_id = $id_alumni;
                $study->save(); 

                $work = work::where('graduate_id', $graduate->id)->first();
                $work->delete();
            
            return redirect()->route('manajemen_alumni.index')->with('success', 'Status bekerja dihapus dan status kuliah telah ditambahkan');

        }else {
            $graduate->keterangan = "kuliah";
            $graduate->push();
            $id_alumni = $graduate->id;
            $study = new Study;
                $study->nma_prgu = $request->nma_prgu;
                $study->jurusan = $request->jurusan;
                $study->mulai = $request->mulai;
                $study->status_id = $request->status_id;
                $study->graduate_id = $id_alumni;
                $study->save(); 

            return redirect()->route('manajemen_alumni.index')->with('success', 'Status kuliah telah ditambahkan');
        }
        
    }

    public function editKuliah($id)
    {
        $website = Website::first();
        $graduate = Graduate::find($id);
        $study = Study::where('graduate_id', $graduate->id)->first();
        $statuses = Status::all();
        return view('backend.manajemen_alumni.editKuliah', compact('website', 'graduate', 'study', 'statuses'));
    }

    public function updateKuliah(Request $request, $id)
    {
        // dd($request->all());
        $study = Study::find($id);
        $study->nma_prgu = $request->nma_prgu;
        $study->jurusan = $request->jurusan;
        $study->mulai = $request->mulai;
        $study->status_id = $request->status_id;
        $study->push(); 

        return redirect()->route('manajemen_alumni.index')->with('success', 'Status kuliah telah diperbaharui');
    }


    //status wirausaha
    public function pilihWirausaha($id)
    {
        $website = Website::first();
        $graduate = Graduate::find($id);
        $incomes = Income::all();
        $statuses = Status::all();
        return view('backend.manajemen_alumni.wirausaha', compact('website', 'graduate', 'statuses', 'incomes'));
    }

    public function wirausaha(Request $request, $id)
    {
        // dd($request->all());
        $graduate = Graduate::find($id);
        $study = Study::where('graduate_id', $graduate->id)->get();
        $work = Work::where('graduate_id', $graduate->id)->get();
        if (!$study->isEmpty()) {
            
            $graduate->keterangan = "wirausaha";
            $graduate->push();
            $id_alumni = $graduate->id;
            $business = new Business;
                $business->nma_pru = $request->nma_pru;
                $business->jenis = $request->jenis;
                $business->mulai = $request->mulai;
                $business->status_id = $request->status_id;
                $business->income_id = $request->income_id;
                $business->graduate_id = $id_alumni;
                $business->save(); 

                $study = Study::where('graduate_id', $graduate->id)->first();
                $study->delete();
    
            return redirect()->route('manajemen_alumni.index')->with('success', 'Status kuliah dihapus dan status wirausaha telah ditambahkan');

        }elseif (!$work->isEmpty()) {
             
            $graduate->keterangan = "wirausaha";
            $graduate->push();
            $id_alumni = $graduate->id;
            $business = new Business;
                $business->nma_pru = $request->nma_pru;
                $business->jenis = $request->jenis;
                $business->mulai = $request->mulai;
                $business->status_id = $request->status_id;
                $business->income_id = $request->income_id;
                $business->graduate_id = $id_alumni;
                $business->save(); 

                $work = Work::where('graduate_id', $graduate->id)->first();
                $work->delete();
    
            return redirect()->route('manajemen_alumni.index')->with('success', 'Status bekerja dihapus dan status wirausaha telah ditambahkan');

        }else {

            $graduate->keterangan = "wirausaha";
            $graduate->push();
            $id_alumni = $graduate->id;
            $business = new Business;
                $business->nma_pru = $request->nma_pru;
                $business->jenis = $request->jenis;
                $business->mulai = $request->mulai;
                $business->status_id = $request->status_id;
                $business->income_id = $request->income_id;
                $business->graduate_id = $id_alumni;
                $business->save(); 
    
            return redirect()->route('manajemen_alumni.index')->with('success', 'Status wirausaha telah ditambahkan');
        }

    }

    public function editWirausaha($id)
    {
        $website = Website::first();
        $graduate = Graduate::find($id);
        $business = Business::where('graduate_id', $graduate->id)->first();
        $incomes = Income::all();
        $statuses = Status::all();
        return view('backend.manajemen_alumni.editWirausaha', compact('website', 'graduate', 'business', 'statuses', 'incomes'));
    }


    public function updateWirausaha(Request $request, $id)
    {
        // dd($request->all());
        $business = Business::find($id);
        $business->nma_pru = $request->nma_pru;
        $business->jenis = $request->jenis;
        $business->mulai = $request->mulai;
        $business->status_id = $request->status_id;
        $business->income_id = $request->income_id;
        $business->push(); 

        return redirect()->route('manajemen_alumni.index')->with('success', 'Status wirausaha telah diperbaharui');
    }


    //status bekerja
    public function pilihBekerja($id)
    {
        $website = Website::first();
        $graduate = Graduate::find($id);
        $incomes = Income::all();
        $statuses = Status::all();
        $locations = Location::all();
        return view('backend.manajemen_alumni.bekerja', compact('website', 'graduate', 'incomes', 'statuses', 'locations'));
    }

    public function bekerja(Request $request, $id)
    {
        // dd($request->all());
        $graduate = Graduate::find($id);
        $study = Study::where('graduate_id', $graduate->id)->get();
        $business = Business::where('graduate_id', $graduate->id)->get();

        if (!$study->isEmpty()) {
            
            $graduate->keterangan = "bekerja";
            $graduate->push();
            $id_alumni = $graduate->id;
            $work = new Work;
            $work->nma_pru = $request->nma_pru;
            $work->posisi = $request->posisi;
            $work->mulai = $request->mulai;
            $work->status_id = $request->status_id;
            $work->sesu_bidkeah = $request->sesu_bidkeah;
            $work->perjanjian = $request->perjanjian;
            $work->bidus = $request->bidus;
            $work->location = $request->location;
            $work->income_id = $request->income_id;
            $work->graduate_id = $id_alumni;
            $work->save(); 

                $study = Study::where('graduate_id', $graduate->id)->first();
                $study->delete();
    
            return redirect()->route('manajemen_alumni.index')->with('success', 'Status kuliah dihapus dan status bekerja telah ditambahkan');

        }elseif (!$business->isEmpty()) {

            $graduate->keterangan = "bekerja";
            $graduate->push();
            $id_alumni = $graduate->id;
            $work = new Work;
            $work->nma_pru = $request->nma_pru;
            $work->posisi = $request->posisi;
            $work->mulai = $request->mulai;
            $work->status_id = $request->status_id;
            $work->sesu_bidkeah = $request->sesu_bidkeah;
            $work->perjanjian = $request->perjanjian;
            $work->bidus = $request->bidus;
            $work->location = $request->location;
            $work->income_id = $request->income_id;
            $work->graduate_id = $id_alumni;
            $work->save(); 

                $business = Business::where('graduate_id', $graduate->id)->first();
                $business->delete();
    
            return redirect()->route('manajemen_alumni.index')->with('success', 'Status wirausaha dihapus dan status bekerja telah ditambahkan');

        }else {
             
            $graduate->keterangan = "bekerja";
            $graduate->push();
            $id_alumni = $graduate->id;
            $work = new Work;
            $work->nma_pru = $request->nma_pru;
            $work->posisi = $request->posisi;
            $work->mulai = $request->mulai;
            $work->status_id = $request->status_id;
            $work->sesu_bidkeah = $request->sesu_bidkeah;
            $work->perjanjian = $request->perjanjian;
            $work->bidus = $request->bidus;
            $work->location = $request->location;
            $work->income_id = $request->income_id;
            $work->graduate_id = $id_alumni;
            $work->save();
    
            return redirect()->route('manajemen_alumni.index')->with('success', 'Status bekerja telah ditambahkan');

        }
        

    }

    public function editBekerja($id)
    {
        $website = Website::first();
        $graduate = Graduate::find($id);
        $work = Work::where('graduate_id', $graduate->id)->first();
        $incomes = Income::all();
        $statuses = Status::all();
        $locations = Location::all();
        return view('backend.manajemen_alumni.editBekerja', compact('website', 'graduate', 'work', 'statuses', 'locations', 'incomes'));
    }


    public function updateBekerja(Request $request, $id)
    {
        // dd($request->all());
        $work = Work::find($id);
        $work->nma_pru = $request->nma_pru;
        $work->posisi = $request->posisi;
        $work->mulai = $request->mulai;
        $work->status_id = $request->status_id;
        $work->sesu_bidkeah = $request->sesu_bidkeah;
        $work->perjanjian = $request->perjanjian;
        $work->bidus = $request->bidus;
        $work->location = $request->location;
        $work->income_id = $request->income_id;
        $work->push(); 

        return redirect()->route('manajemen_alumni.index')->with('success', 'Status bekerja telah diperbaharui');
    }
}
