<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Graduate;
use Illuminate\Support\Facades\Hash;
use App\Study;
use App\Business;
use App\Work;
use App\Website;

class ManajemenAlumniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $website = Website::first();
        $graduates = Graduate::all();
        $studies = Study::all();
        $business = Business::all();
        

        // dd($ket, $cekstudy, $cekgraduate);
        
        return view('backend.manajemen_alumni.index', compact('website', 'graduates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $graduate = new Graduate;
            $graduate->name = $request->name;
            $graduate->nisn = $request->nisn;
            $graduate->th_lulus = $request->th_lulus;
            $graduate->department = $request->department;
            $graduate->keterangan = "belum ada";
            $graduate->save(); 

        return redirect()->route('manajemen_alumni.index')->with('success', 'Data telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $website = Website::first();
        $graduate = Graduate::find($id);
        $idg = $graduate->id; 
        $study = Study::where('graduate_id', $idg)->first();
        $business = Business::where('graduate_id', $idg)->first();
        $work = Work::where('graduate_id', $idg)->first();
        return view('backend.manajemen_alumni.show', compact('website', 'graduate', 'study', 'business', 'work'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $website = Website::first();
        $graduate = Graduate::find($id);
        return view('backend.manajemen_alumni.edit', compact('website', 'graduate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $graduate = Graduate::find($id);
        $graduate->name = $request->name;
        $graduate->nisn = $request->nisn;
        $graduate->th_lulus = $request->th_lulus;
        $graduate->department = $request->department;
        $graduate->push(); 
        

        return redirect()->route('manajemen_alumni.index')->with('update', 'Data telah diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $graduate = Graduate::find($id);
        
        $study = Study::where('graduate_id', $graduate->id)->get();
        $business = Business::where('graduate_id', $graduate->id)->get();
        $work = Work::where('graduate_id', $graduate->id)->get();
        if (!$study->isEmpty()) {
            
            $study = Study::where('graduate_id', $graduate->id)->first();
            $study->delete();

        }elseif (!$business->isEmpty()) {

            $business = Business::where('graduate_id', $graduate->id)->first();
            $business->delete();

        }elseif (!$work->isEmpty()) {
             
            $work = Work::where('graduate_id', $graduate->id)->first();
            $work->delete();

        }
        
            $graduate->delete(); 
            return redirect()->route('manajemen_alumni.index')->with('delete', 'Data telah dihapus');
        
    }

    
}
