<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Loker;
use App\Category;
use App\Website;
use File;

class BursaKerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $website = Website::first();
        $lokers = Loker::orderBy('id', 'DESC')->get();
        $categories = Category::all();
        return view('backend.bursa_kerja.index', compact('website', 'lokers', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $destination = "public/brosurKerja";

        $brosur = $request->file('brosur');
        $extension = $brosur->getClientOriginalExtension(); 
        // RENAME THE UPLOAD WITH RANDOM NUMBER 
        $brosurName = rand(11111, 33333) . '.' . $extension; 
        // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
        $brosur->move($destination, $brosurName);

        $loker = new Loker;
        $loker->title = $request->title;
        $loker->deskripsi = $request->deskripsi;
        $loker->perusahaan = $request->perusahaan;
        $loker->category_id = $request->category_id;
        $loker->tgl_awal = $request->tgl_awal;
        $loker->tgl_akhir = $request->tgl_akhir;
        $loker->brosur = $brosurName;
        $loker->save(); 

        return redirect()->route('bursa_kerja.index')->with('success', 'Data telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $website = Website::first();
        $loker = Loker::find($id);
        return view('backend.bursa_kerja.show', compact('website', 'loker'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $website = Website::first();
        $loker = Loker::find($id);
        $categories = Category::all();
        // dd($loker);
        return view('backend.bursa_kerja.edit', compact('website', 'loker', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->brosur == null)
        {

            //$idUser = Auth::user()->id;
            //$tgl_wak = Carbon::now()->toDateTimeString('d-M-Y H:i');


            $loker = Loker::find($id);;
            $loker->title = $request->title;
            $loker->deskripsi = $request->deskripsi;
            $loker->perusahaan = $request->perusahaan;
            $loker->category_id = $request->category_id;
            $loker->tgl_awal = $request->tgl_awal;
            $loker->tgl_akhir = $request->tgl_akhir;
            $loker->push();

        }else
        {
            //$idUser = Auth::user()->id;
            //$tgl_wak = Carbon::now()->toDateTimeString('d-M-Y H:i');
            

            $destination = "public/brosurKerja";

            $brosur = $request->file('brosur');
            $extension = $brosur->getClientOriginalExtension(); 
            // RENAME THE UPLOAD WITH RANDOM NUMBER 
            $brosurName = rand(11111, 33333) . '.' . $extension; 
            // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
            $brosur->move($destination, $brosurName);

            // hapus file
            $gambar = Loker::where('id',$id)->first();
            File::delete('public/brosurKerja/'.$gambar->brosur);

            $loker = Loker::find($id);
            $loker->title = $request->title;
            $loker->deskripsi = $request->deskripsi;
            $loker->perusahaan = $request->perusahaan;
            $loker->category_id = $request->category_id;
            $loker->tgl_awal = $request->tgl_awal;
            $loker->tgl_akhir = $request->tgl_akhir;
            $loker->brosur = $brosurName;
            $loker->push(); 
        }

        return redirect()->route('bursa_kerja.index')->with('update', 'Data telah diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // hapus file
		$loker = Loker::where('id',$id)->first();
		File::delete('public/brosurKerja/'.$loker->brosur);
 
		// hapus data
        Loker::destroy($id);

        return redirect()->route('bursa_kerja.index')->with('delete', 'Data telah dihapus');
    }
}
