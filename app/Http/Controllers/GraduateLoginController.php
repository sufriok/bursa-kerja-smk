<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Website;
use App\Category;
use App\Status;
use App\Income; 
use App\Location;
use App\Graduate;
use App\Study;
use App\Business;
use App\Work;

class GraduateLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/graduate/profil';

    public function __construct()
    {
        $this->middleware('guest:graduate')->except('logout')->except('index');
    }

    public function index(){
        
        $website = Website::first();
        $id = Auth::id();
        $statuses = Status::all();
        $incomes = Income::all();
        $locations = Location::all();
        $categories = Category::all();
        $graduate = Graduate::where('id', $id)->first();
        $study = Study::where('graduate_id', $id)->first();
        $business = Business::where('graduate_id', $id)->first();
        $work = Work::where('graduate_id', $id)->first();
        return view('frontend.auth.profil', compact('website','graduate', 'study', 'business', 'work', 'statuses', 'incomes', 'locations', 'categories'));
        
    }

    public function showLoginForm()
    {
        $website = Website::first();
        $categories = Category::all();
        return view('frontend.auth.login', compact('website', 'categories'));
    }

    public function showRegisterForm()
    {
        return view('frontend.auth.register');
    }

    public function username()
    {
        $login = request()->input('nisn');
        $field = 'nisn';
        // if(is_numeric($login)){
        //     $field = 'nisn';
        // } elseif (filter_var($login, FILTER_VALIDATE_EMAIL)) {
        //     $field = 'email';
        // } else {
        //     $field = 'name';
        // }
        request()->merge([$field => $login]);
        return $field;    
        //return 'name';
    }

    protected function guard()
    {
        return Auth::guard('graduate');
    }

    public function register(Request $request)
    {
        
        $graduate = new Graduate;
            $graduate->name = $request->name;
            $graduate->nisn = $request->nisn;
            $graduate->email = $request->email;
            $graduate->password = Hash::make($request->password);
            $graduate->th_lulus = "2018";
            $graduate->department_id = "1";
            $graduate->save();

        return redirect()->route('graduate.registerform')->with('success', 'Successfully register!');
    }

}
