<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Graduate;
use App\Website;

class AkunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $website = Website::first();
        $users = User::where('status', "2")->get();
        return view('backend.akun.index', compact('website', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->status = "2";
            $user->save(); 

        return redirect()->route('akun.index')->with('success', 'Data telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $graduate = Graduate::where('user_id', $id)->first();
        // $graduate->user_id = "";
        // $graduate->push();

        // $users = User::where('status', "2")->get();
        // return view('backend.akun.index', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $website = Website::first();
        $user = User::find($id);
        // $graduate = Graduate::where('user_id', $id)->first();
        return view('backend.akun.edit', compact('website', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->password == null)
        {
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = $user->password; 
            $user->push(); 
        }else
        {
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->push(); 
        }

        return redirect()->route('akun.index')->with('update', 'Data telah diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('akun.index')->with('delete', 'Data telah dihapus');
    }

    public function hapusGraduate($id)
    {
        $graduate = Graduate::where('user_id', $id)->firstOrFail();
        $graduate->user_id = null;
        $graduate->push();

        $users = User::where('status', "2")->get();
        return view('backend.akun.index', compact('users'));
    }
}
