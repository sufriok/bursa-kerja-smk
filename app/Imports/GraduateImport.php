<?php

namespace App\Imports;

use App\Graduate;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class GraduateImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // $nameg = Graduate::pluck('nisn')->all();
        // dd($nameg);

        if (Graduate::where('nisn', $row['nisn'])->exists()) {
            return null;

        }else {
            return new Graduate([
                'name' => $row['nama'],
                'nisn' => $row['nisn'], 
                // 'email' => $row['nisn']."@test.com",
                // 'password' => Hash::make("siswa"), 
                'th_lulus' => $row['tahun_lulus'], 
                'department' => $row['jurusan'], 
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ]);

        }
        
    }

}
