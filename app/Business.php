<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Status;
use App\Income;

class Business extends Model
{
    protected $fillable = [
        'nma_pru',
        'jenis',
        'mulai',
        'status_id',
        'income_id',
        'graduate_id'
    ];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function income()
    {
        return $this->belongsTo(Income::class);
    }
}
