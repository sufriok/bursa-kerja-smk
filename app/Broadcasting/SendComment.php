<?php

namespace App\Broadcasting;


use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;


use App\User;
use App\Comment;

class SendComment implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $comment;
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    public function broadcastWith()
    {
        return [
            'content' => $this->comment->content,
            'user' => [
                'name' => $this->comment->user->name
            ],
        ];
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param  \App\User  $user
     * @return array|bool
     */
    // public function join(User $user)
    // {
    //     
    // }

    public function broadcastOn()
    {
        return new PrivateChannel('post');
    }
}
