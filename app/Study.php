<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Status;
use App\Income;

class Study extends Model
{
    protected $fillable = [
        'nma_prgu',
        'jurusan',
        'mulai',
        'status_id',
        'graduate_id'
    ];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function income()
    {
        return $this->belongsTo(Income::class);
    }
}
