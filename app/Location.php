<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Work;

class Location extends Model
{
    protected $fillable = ['lokasi'];

    public function work()
    {
        return $this->hasMany(Work::class, 'location_id', 'id');
    }
}
