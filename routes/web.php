<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@beranda')->name('beranda');


Route::get('/all_loker', 'FrontendController@showAllLoker')->name('show.all'); 
Route::get('/cari_loker', 'FrontendController@cariLoker')->name('cari.loker');
Route::get('/show_loker/{loker}', 'FrontendController@showLoker')->name('show.loker');
Route::get('/kategori/{category}', 'FrontendController@categoryLoker')->name('category.loker');


Auth::routes();



Route::middleware('auth')->group(function(){

//auth frontend
Route::get('/home', 'HomeController@index')->name('home');
Route::delete('/message/hapus/{message}', 'HomeController@destroyPesan')->name('message.hapus');
Route::delete('/post/hapus/{post}', 'HomeController@destroyPost')->name('post.hapus');
Route::resource('/chatroom', 'ChatRoomController');
Route::post('/comment/{post}', 'CommentController@store')->name('comment.store');
Route::get('/comment/{post}', 'CommentController@index');

Route::PATCH('/profil/update/{id}', 'FrontendController@updateProfil')->name('updateProfil');

Route::PATCH('/kirim_pesan', 'FrontendController@kirimMessage')->name('kirim.message');
Route::get('/registerGraduate', 'FrontendController@registerGraduate')->name('registerGraduate');
Route::get('/pilihGraduate/{pilih}', 'FrontendController@pilihGraduate')->name('pilihGraduate');

//kuliah
Route::get('/profil/{graduate}/kuliah', 'FrontendController@ketKuliah')->name('ket.kuliah');
Route::PATCH('/profil/kuliah/{graduate}', 'FrontendController@profilKuliah')->name('profil.kuliah');
Route::PATCH('/profil-kuliah/update/{study}', 'FrontendController@updateProfilKuliah')->name('updateProfil.kuliah');
//wirausaha
Route::get('/profil/{graduate}/wirausaha', 'FrontendController@ketWirausaha')->name('ket.wirausaha');
Route::PATCH('/profil/wirausaha/{graduate}', 'FrontendController@profilWirausaha')->name('profil.wirausaha');
Route::PATCH('/profil-wirausaha/update/{business}', 'FrontendController@updateProfilWirausaha')->name('updateProfil.wirausaha');
//bekerja
Route::get('/profil/{graduate}/bekerja', 'FrontendController@ketBekerja')->name('ket.bekerja');
Route::PATCH('/profil/bekerja/{graduate}', 'FrontendController@profilBekerja')->name('profil.bekerja');
Route::PATCH('/profil-bekerja/update/{work}', 'FrontendController@updateProfilBekerja')->name('updateProfil.bekerja');

//auth admin
Route::resource('/admin', 'AdminController');
Route::resource('/manajemen_alumni', 'ManajemenAlumniController');
Route::resource('/akun', 'AkunController');
Route::get('/akun/{user}/hapusgraduate', 'AkunController@hapusGraduate')->name('hapus.graduate');

//keterangan alumni kuliah
Route::get('/graduate/{graduate}/kuliah', 'KeteranganController@pilihKuliah')->name('pilih.kuliah');
Route::PATCH('/graduate/kuliah/{graduate}', 'KeteranganController@kuliah')->name('kuliah');
Route::get('/kuliah/{graduate}/edit', 'KeteranganController@editKuliah')->name('edit.kuliah');
Route::PATCH('/kuliah/update/{study}', 'KeteranganController@updateKuliah')->name('update.kuliah');

//keterangan alumni wirausaha
Route::get('/graduate/{graduate}/wirausaha', 'KeteranganController@pilihWirausaha')->name('pilih.wirausaha');
Route::PATCH('/graduate/wirausaha/{graduate}', 'KeteranganController@wirausaha')->name('wirausaha');
Route::get('/wirausaha/{graduate}/edit', 'KeteranganController@editWirausaha')->name('edit.wirausaha');
Route::PATCH('/wirausaha/update/{business}', 'KeteranganController@updateWirausaha')->name('update.wirausaha');

//keterangan alumni bekerja
Route::get('/graduate/{graduate}/bekerja', 'KeteranganController@pilihBekerja')->name('pilih.bekerja');
Route::PATCH('/graduate/bekerja/{graduate}', 'KeteranganController@bekerja')->name('bekerja');
Route::get('/bekerja/{graduate}/edit', 'KeteranganController@editBekerja')->name('edit.bekerja');
Route::PATCH('/bekerja/update/{work}', 'KeteranganController@updateBekerja')->name('update.bekerja');

Route::get('/Graduate/export_excel', 'ExportImportController@export_excel')->name('graduate.export');
Route::post('/Graduate/import_excel', 'ExportImportController@import_excel')->name('graduate.import');
Route::resource('/bursa_kerja', 'BursaKerjaController');
Route::resource('/category', 'CategoryController');
Route::resource('/mou', 'MouController');
//manajemen website
Route::get('/manajemen_website', 'WebsiteController@index')->name('manajemen.website');
Route::PATCH('/manajemen_website/update/', 'WebsiteController@update')->name('update.website');

});


// Route::get('/graduate/login', 'GraduateLoginController@showLoginForm')->name('graduate.loginform');
// Route::get('/graduate/register', 'GraduateLoginController@showRegisterForm')->name('graduate.registerform');
// Route::post('/graduate/login', 'GraduateLoginController@login')->name('graduate.login');
// Route::post('/graduate/register', 'GraduateLoginController@register')->name('graduate.register');
// Route::get('/graduate/profil', 'GraduateLoginController@index')->middleware('auth:graduate')->name('profil.alumni');
// Route::get('/graduate/logout', 'GraduateLoginController@logout')->name('graduate.logout');


// Route::PATCH('/profil/kuliah/{auth}', 'KeteranganController@kuliah')->name('submit.kuliah');

