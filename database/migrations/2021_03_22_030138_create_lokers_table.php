<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLokersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lokers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->longText('deskripsi');
            $table->string('perusahaan');//client
            $table->string('tgl_awal');
            $table->string('tgl_akhir');
            $table->integer('category_id')->unsigned();
            $table->string('brosur');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lokers');
    }
}
