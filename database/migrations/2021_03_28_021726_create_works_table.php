<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->bigIncrements('id'); 
            $table->string('nma_pru');
            $table->string('posisi');
            $table->string('mulai');
            $table->integer('status_id')->unsigned();
            $table->enum('sesu_bidkeah', ['ya', 'tidak']);
            $table->enum('perjanjian', ['kontrak', 'tetap']);
            $table->string('bidus');//bidang usaha
            $table->string('location');//lokasi penempatan
            $table->integer('income_id')->unsigned();
            $table->integer('graduate_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
