<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('websites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('header_logo');
            $table->string('header_title');
            $table->string('header_kalimat1');
            $table->string('header_kalimat2');
            $table->string('about_title');
            $table->longText('about_deskripsi');
            $table->string('about_foto');
            $table->longText('contact_alamat');
            $table->string('contact_email');
            $table->string('contact_phone'); 
            $table->string('footer_twitter')->nullable();
            $table->string('footer_facebook')->nullable();
            $table->string('footer_instagram')->nullable();
            $table->string('footer_youtube')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('websites');
    }
}
