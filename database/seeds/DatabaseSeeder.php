<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            
            UsersTableSeeder::class,
            GraduatesTableSeeder::class,
            IncomesTableSeeder::class,
            CategoriesTableSeeder::class,
            StatusesTableSeeder::class,
            MousTableSeeder::class,
            LokersTableSeeder::class,
            WebsitesTableSeeder::class,

        ]);
        
    }
}
