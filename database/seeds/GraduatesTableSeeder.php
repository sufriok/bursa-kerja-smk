<?php

use Illuminate\Database\Seeder;

class GraduatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 6; $i++) { 
            DB::table('graduates')->insert([
               'name' => "alumni0$i",
               'nisn' => "097860$i",
               'th_lulus' => "2018",
               'department' => "Rekayasa Perangkat Lunak",
               'created_at'=>date('Y-m-d H:i:s'),
               'updated_at'=>date('Y-m-d H:i:s'),
   
           ]);
       }
    }
}
