<?php

use Illuminate\Database\Seeder;

class IncomesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('incomes')->insert([
            [
                'nominal' => "< 1.000.000",
            ],
            
            [
                'nominal' => "1.000.000 - 2.000.000",
            ],

            [
                'nominal' => "2.000.000 - 3.000.000",
            ],

            [
                'nominal' => "3.000.000 - 4.000.000",
            ],

            [
                'nominal' => "4.000.000 - 5.000.000",
            ],
            
            [
                'nominal' => "> 5.000.000",
            ],
        ]);
    }
}
