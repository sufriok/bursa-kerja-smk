<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->insert([
            [
                'lokasi' => "Luar Negeri",
            ],
            
            [
                'lokasi' => "Prov. DKI Jakarta",
            ],

            [
                'lokasi' => "Prov. DI Yogyakarta",
            ],

            [
                'lokasi' => "Prov. Jawa Barat",
            ],

            [
                'lokasi' => "Prov. Jawa Tengah",
            ],
            
            [
                'lokasi' => "Prov. Jawa Timur",
            ],

            [
                'lokasi' => "Prov. Kalimantan Barat",
            ],

            [
                'lokasi' => "Prov. Kalimantan Timur",
            ],

            [
                'lokasi' => "Prov. Kalimantan Tengah",
            ],

            [
                'lokasi' => "Prov. Kalimantan Selatan",
            ],

            [
                'lokasi' => "Prov. Kalimantan Utara",
            ],

        ]);
    }
}
