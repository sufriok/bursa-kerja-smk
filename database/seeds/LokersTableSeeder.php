<?php

use Illuminate\Database\Seeder;

class LokersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 6; $i++) { 

            if ($i <= 2) {
                $category = "1";
               }elseif ($i > 2 && $i <= 4) {
                $category = "2";
               }else {
                $category = "3";
               }
               
            DB::table('lokers')->insert([
               'title' => "Lorem ipsum dolor sit amet",
               'deskripsi' => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus, dignissimos numquam aperiam nihil cupiditate vitae aspernatur perspiciatis iusto totam sunt quae commodi ipsum! Recusandae sequi sed provident eius, in pariatur.",
               'Perusahaan' => "PT. Indomaret",
               'tgl_awal'=>date('Y-m-d H:i:s'),
               'tgl_akhir'=>date('Y-m-d H:i:s'),
               'category_id' => $category,
               'brosur' => "bosurkerja-$i.jpg",
               'created_at'=>date('Y-m-d H:i:s'),
               'updated_at'=>date('Y-m-d H:i:s'),
   
           ]);
       }
    }
}
