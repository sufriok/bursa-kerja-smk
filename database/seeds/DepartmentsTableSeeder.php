<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            [
                'name' => "Teknik Komputer dan Jaringan (TKJ)",
            ],
            
            [
                'name' => "Rekayasa Perangkat Lunak (RPL)",
            ],

            [
                'name' => "Multimedia (MM)",
            ]
            
        ]);
    }
}
