<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => "admin1",
                'email' => "admin1@test.com",
                'password' => Hash::make('admin11234'),
                'status' => "1",
            ],
            
            [
                'name' => "admin2",
                'email' => "admin2@test.com",
                'password' => Hash::make('admin21234'),
                'status' => "1",
            ],

            [
                'name' => "alumni1",
                'email' => "alumni1@test.com",
                'password' => Hash::make('alumni11234'),
                'status' => "2",
            ],

            [
                'name' => "alumni2",
                'email' => "alumni2@test.com",
                'password' => Hash::make('alumni21234'),
                'status' => "2",
            ]
            
        ]);
    }
}
