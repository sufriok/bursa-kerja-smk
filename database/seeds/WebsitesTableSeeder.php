<?php

use Illuminate\Database\Seeder;

class WebsitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('websites')->insert([
            [
                'header_logo' => "logobkk.png",
                'header_title' => "BKK SMKN 1 Tebas",
                'header_kalimat1' => "Bursa Kerja Khusus (BKK) SMK Negeri 1 Tebas",
                'header_kalimat2' => "Selamat Datang Di Website Resmi Kami..!",
                'about_title' => "Bursa Kerja Khusus SMK Negeri 1 Tebas",
                'about_deskripsi' => "Bursa Kerja Khusus (BKK) adalah sebuah lembaga yang dibentuk di Sekolah Menengah Kejuruan Negeri dan Swasta, sebagai unit pelaksana yang memberikan pelayanan dan informasi lowongan kerja, pelaksana pemasaran, penyaluran dan penempatan tenaga kerja, merupakan mitra Dinas Tenaga Kerja dan Transmigrasi.",
                'about_foto' => "work-staff.jpg",
                'contact_alamat' => "Jalan H. Said, Dsn. Melati, Ds. Tebas Sungai, Kec. Tebas, Kode Pos 79461",
                'contact_email' => "smkn1tebas@gmail.com",
                'contact_phone' => "(0562) 371-221",
                'footer_twitter' => "#",
                'footer_facebook' => "https://www.facebook.com/smkn1tebas/",
                'footer_instagram' => "https://www.instagram.com/smkn1tebas/",
                'footer_youtube' => "https://www.youtube.com/channel/UCz9ATs2ZbTaDrg-qp16RJ9A",
            ],
        ]);
    }
}
