<?php

use Illuminate\Database\Seeder;

class MousTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for ($i=1; $i < 9; $i++) { 
            DB::table('mous')->insert([
               'perusahaan' => "perusahaan-$i",
               'logo' => "client-$i.png",
               'created_at'=>date('Y-m-d H:i:s'),
               'updated_at'=>date('Y-m-d H:i:s'),
   
           ]);
       }
    }
}
