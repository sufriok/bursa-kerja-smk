@extends('frontend.layouts.main')

@section('content')
<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <!-- <ol>
          <li><a href="/">Beranda</a></li>
          <li>Login</li>
        </ol>
        <h5>Login BKK</h5> -->

      </div>
    </section><!-- End Breadcrumbs -->
<!-- <br>
    @if(Session::has('success'))
        <div class="alert alert-info alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif
    @if(Session::has('update'))
        <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('update') }}</strong>
        </div>
    @endif
    @if(Session::has('delete'))
        <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('delete') }}</strong>
        </div>
    @endif
<br> -->
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">

      <div class="container" data-aos="fade-up">

        <!-- <header class="section-header">
          <h2>Contact</h2>
          <p>Contact Us</p>
        </header> -->

        <div class="row gy-4 m-auto">

          <div class="col-lg-3">

            <!-- <div class="row gy-4">
              <div class="col">
                <div class="info-box">
                  <center>
                  <img src=""
                            class="rounded-circle" width="150" />
                  
                  <h3></h3>
                  <p>NISN:<br>
                  Angkatan:<br>
                  </p>                
            
                  </center>
                </div>
              </div>
            </div> -->

          </div>

          <div class="col-lg-6">
            <form action="{{ route('login') }}" method="post" class="form-edit">
            @csrf
              <div class="row gy-4">

                <div class="col-md-12 text-center">
                  <h3>Login BKK</h3>
                </div>

                <div class="col-md-12 ">
                  <h6>Email :</h6>
                  <input type="email" class="form-control  @error('email') is-invalid @enderror" name="email" required>
                  @error('email')
                      <span class="invalid-feedback" role="alert">
                          <small> email atau password anda salah</small>
                      </span>
                  @enderror
                </div>

                <div class="col-md-12">
                  <h6>Password :</h6>
                  <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>
                  @error('password')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>

                <div class="col-md-12 text-center">
                  <button type="submit">Login</button>
                </div>

              </div>
            </form>

          </div>

          <div class="col-lg-3">

            <!-- <div class="row gy-4">
              <div class="col">
                <div class="info-box">
                  <center>
                  <img src=""
                            class="rounded-circle" width="150" />
                  
                  <h3></h3>
                  <p>NISN:<br>
                  Angkatan:<br>
                  </p>                
            
                  </center>
                </div>
              </div>
            </div> -->

          </div>

        </div>
        
        

      </div>

    </section><!-- End Contact Section -->
  </main><!-- End #main -->
@endsection

