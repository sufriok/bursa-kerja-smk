<table>
    <thead>
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>NISN</th>
        <th>Tahun Lulus</th>
        <th>Keterangan</th>
    </tr>
    </thead>
    <tbody>
    @php $i=1 @endphp
    @foreach($graduates as $graduate)
        <tr>
            <td>{{ $i++ }}</td>
            <td>{{ $graduate->name }}</td>
            <td>{{ $graduate->nisn }}</td>
            <td>{{ $graduate->th_lulus }}</td>
            <td>{{ $graduate->keterangan }}</td>
        </tr>
    @endforeach
    </tbody>
</table>