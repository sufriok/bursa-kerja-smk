@extends('backend.layouts.main')

@section('content')
<div class="page-wrapper mt-5 pt-4">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
    @if(Session::has('success'))
        <div class="alert alert-info alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif
    @if(Session::has('update'))
        <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('update') }}</strong>
        </div>
    @endif
    @if(Session::has('delete'))
        <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('delete') }}</strong>
        </div>
    @endif
        <div class="row align-items-center">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="page-title mb-0 p-0">M.O.U</h3>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">M.O.U</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <div class="text-end upgrade-btn">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success d-md-inline-block text-white" data-toggle="modal" data-target="#tambahMOU">
                        Tambah
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <h4 class="card-title">Basic Table</h4>
                        <h6 class="card-subtitle">Add class <code>.table</code></h6> -->
                        <div class="table-responsive">
                            <table class="table user-table table-bordered" id="example-ok">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">#</th>
                                        <th class="border-top-0">Nama Perusahaan</th>
                                        <th class="border-top-0">Logo</th>
                                        <th class="border-top-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                @php $i=1 @endphp
                                    @foreach($mous as $mou)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $mou->perusahaan }}</td>
                                        <td><img src="{{ asset('/logoMou/'.$mou->logo) }}" class="img-fluid img-responsive"  width="250" height="120"></td>
                                        <td>
                                            <form action="{{ route('mou.destroy', $mou) }}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <a class="btn btn-success" href="{{ route('mou.edit', $mou) }}"><i class="fa fa-edit fa-fw"></i></a>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data?')"><i class="fa fa-trash fa-fw"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        © 2021 Bursa Kerja Khusus by <a href="https://smkn1tebas.sch.id/">smkn1tebas.sch.id</a>
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

<!-- Modal -->
<div class="modal fade" id="tambahMOU" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-secondary text-white">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data MOU</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{ route('mou.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">

          <div class="row">
            <div class="col">
              <div class="form-group">
                <label for="exampleInputNama">Nama Perusahaan</label>
                <input type="text" name="perusahaan" class="form-control" id="exampleInputNama" required>
                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
              </div>
              <div class="form-group">
                <label for="exampleLogo">Logo Perusahaan</label>
                <input type="file" name="logo" class="form-control" id="exampleLogo" required>
              </div>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
          <button type="Submit" class="btn btn-info">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection