<aside class="left-sidebar position-fixed" data-sidebarbg="skin6">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                        href="{{ route('home') }}" aria-expanded="false"><i class="me-3 far fa-clock fa-fw"
                            aria-hidden="true"></i><span class="hide-menu">Dashboard</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                        href="{{ route('manajemen.website') }}" aria-expanded="false">
                        <i class="me-3 fas fa-cogs" aria-hidden="true"></i><span
                            class="hide-menu">Website</span></a>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                        href="{{ route('manajemen_alumni.index') }}" aria-expanded="false">
                        <i class="me-3 fas fa-address-card" aria-hidden="true"></i><span
                            class="hide-menu">Alumni</span></a>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                        href="{{ route('akun.index') }}" aria-expanded="false">
                        <i class="me-3 fas fa-address-book" aria-hidden="true"></i><span
                            class="hide-menu">Akun</span></a>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                        href="{{ route('bursa_kerja.index') }}" aria-expanded="false"><i class="me-3 fa fa-table"
                            aria-hidden="true"></i><span class="hide-menu">Bursa Kerja</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                        href="{{ route('mou.index') }}" aria-expanded="false"><i class="me-3 fa fas fa-handshake"
                            aria-hidden="true"></i><span class="hide-menu">M.O.U</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                        href="{{ route('admin.index') }}" aria-expanded="false"><i class="me-3 fa fa-columns"
                            aria-hidden="true"></i><span class="hide-menu">Admin</span></a></li>
                <!-- <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                        href="blank.html" aria-expanded="false"><i class="me-3 fa fa-columns"
                            aria-hidden="true"></i><span class="hide-menu">Blank</span></a></li> -->
                <!-- <li class="sidebar-item"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link"
                        href="{{ route('logout') }}" aria-expanded="false"
                        aria-hidden="true"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><i class="me-3 fa fas fa-sign-out-alt">
                        </i><span class="hide-menu">Logout</span></a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form></li> -->

                <li class="text-center p-20 upgrade-btn">
                    <!-- <a href="https://www.wrappixel.com/templates/monsteradmin/"
                        class="" target="_blank">Upgrade to
                        Pro</a> -->

                    <a class="btn btn-danger text-white mt-4"
                        href="{{ route('logout') }}" aria-expanded="false"
                        aria-hidden="true"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><i class="me-3 fa fas fa-sign-out-alt">
                        </i><span class="hide-menu">Logout</span></a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form></li>
                </li>
            </ul>

        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>