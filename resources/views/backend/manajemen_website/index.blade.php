@extends('backend.layouts.main')

@section('content')

<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper mt-5 pt-4">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
    @if(Session::has('success'))
        <div class="alert alert-info alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif
    @if(Session::has('update'))
        <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('update') }}</strong>
        </div>
    @endif
    @if(Session::has('delete'))
        <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('delete') }}</strong>
        </div>
    @endif
        <div class="row align-items-center">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="page-title mb-0 p-0">Manajemen Website</h3>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Manajemen Website</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <!-- <div class="text-end upgrade-btn">
                    <a href="https://www.wrappixel.com/templates/monsteradmin/"
                        class="btn btn-success d-none d-md-inline-block text-white" target="_blank">Upgrade to
                        Pro</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown"></ul>
                </div> -->
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        This is some text within a card block.
                    </div>
                </div>
            </div> -->
            <!-- Column -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{ route('update.website') }}" enctype="multipart/form-data" class="form-horizontal form-material mx-2">
                            @csrf
                            @method('PATCH')
                            <div class="form-group">
                                <div class="col-sm-12 text-center">
                                    <button class="btn btn-outline-info mx-auto mx-md-0" disabled>Halaman Header</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0"><strong>Logo</strong></label><br>
                                <div class="col-md-12">
                                <img data-toggle="modal" data-target="#gantiLogo" src="{{ asset('/images/'.$website->header_logo) }}" class="img-fluid img-responsive"  width="150" height="150">   
                                </div>
                            </div>
                            
                            <!-- <div class="form-group">
                                <label class="col-md-12 mb-0">Logo</label>
                                <div class="col-md-12">
                                    <input type="file" value="{{ $website->header_logo }}"
                                        class="form-control ps-0 form-control-line">
                                </div>
                            </div> -->
                            
                            <div class="form-group">
                                <label class="col-md-12 mb-0"><strong>Title Header</strong></label>
                                <div class="col-md-12">
                                    <input type="text" name="header_title" value="{{ $website->header_title }}"
                                        class="form-control ps-0 form-control-line" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0"><strong>Header Kalimat1</strong></label>
                                <div class="col-md-12">
                                    <input type="text" name="header_kalimat1" value="{{ $website->header_kalimat1 }}"
                                        class="form-control ps-0 form-control-line" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0"><strong>Header Kalimat2</strong></label>
                                <div class="col-md-12">
                                    <input type="text" name="header_kalimat2" value="{{ $website->header_kalimat2 }}"
                                        class="form-control ps-0 form-control-line" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 text-center">
                                    <button class="btn btn-outline-info mx-auto mx-md-0" disabled>Halaman About</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0"><strong>About Foto</strong></label><br>
                                <div class="col-md-12">
                                <img data-toggle="modal" data-target="#gantiAboutFoto" src="{{ asset('/images/'.$website->about_foto) }}" class="img-fluid img-responsive"  width="350" height="350">   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0"><strong>Title About</strong></label>
                                <div class="col-md-12">
                                    <input type="text"  name="about_title" value="{{ $website->about_title }}"
                                        class="form-control ps-0 form-control-line" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0"><strong>Deskripsi About</strong></label>
                                <div class="col-md-12">
                                    <textarea rows="3" name="about_deskripsi" class="form-control ps-0 form-control-line">{{ $website->about_deskripsi }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 text-center">
                                    <button class="btn btn-outline-info mx-auto mx-md-0" disabled>Halaman Contact</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0"><strong>Alamat contact</strong></label>
                                <div class="col-md-12">
                                    <textarea rows="1" name="contact_alamat" class="form-control ps-0 form-control-line">{{ $website->contact_alamat }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12"><strong>Email contact</strong></label>
                                <div class="col-md-12">
                                    <input type="email" name="contact_email" value="{{ $website->contact_email }}"
                                        class="form-control ps-0 form-control-line" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0"><strong>Phone contact</strong></label>
                                <div class="col-md-12">
                                    <input type="text" name="contact_phone" value="{{ $website->contact_phone }}"
                                        class="form-control ps-0 form-control-line" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 text-center">
                                    <button class="btn btn-outline-info mx-auto mx-md-0" disabled>Halaman Footer</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0"><strong>Twitter footer</strong></label>
                                <div class="col-md-12">
                                    <input type="text" name="footer_twitter" value="{{ $website->footer_twitter }}"
                                        class="form-control ps-0 form-control-line" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0"><strong>Facebook footer</strong></label>
                                <div class="col-md-12">
                                    <input type="text" name="footer_facebook" value="{{ $website->footer_facebook }}"
                                        class="form-control ps-0 form-control-line" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0"><strong>Instagram footer</strong></label>
                                <div class="col-md-12">
                                    <input type="text" name="footer_instagram" value="{{ $website->footer_instagram }}"
                                        class="form-control ps-0 form-control-line" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0"><strong>Youtube footer</strong></label>
                                <div class="col-md-12">
                                    <input type="text" name="footer_youtube" value="{{ $website->footer_youtube }}"
                                        class="form-control ps-0 form-control-line" required>
                                </div>
                            </div>
                            <div class="form-group float-right">
                                <div class="col-sm-12 ">
                                    <button class="btn btn-success mx-auto mx-md-0 text-white">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        © 2021 Bursa Kerja Khusus by <a href="https://smkn1tebas.sch.id/">smkn1tebas.sch.id</a>
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->

<!-- Modal Logo-->
<div class="modal fade" id="gantiLogo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-secondary text-white">
        <h5 class="modal-title" id="exampleModalLabel">Update Header Logo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{ route('update.website') }}" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="modal-body">

          <div class="row">
            <div class="col-6">
              
              <div class="form-group">
                <label for="e">Header Logo</label>
                <input type="file" name="logo" class="form-control" id="" required>
              </div>
    
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
          <button type="Submit" class="btn btn-info">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal About Foto-->
<div class="modal fade" id="gantiAboutFoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-secondary text-white">
        <h5 class="modal-title" id="exampleModalLabel">Update About Foto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{ route('update.website') }}" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="modal-body">

          <div class="row">
            <div class="col-6">
              
              <div class="form-group">
                <label for="e">About Foto</label>
                <input type="file" name="about_foto" class="form-control" id="" required>
              </div>
    
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
          <button type="Submit" class="btn btn-info">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection