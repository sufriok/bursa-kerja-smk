@extends('backend.layouts.main')

@section('content')
<div class="page-wrapper mt-5 pt-4">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row align-items-center">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="page-title mb-0 p-0">Edit Keterangan Bekerja</h3>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Keterangan Bekerja</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <div class="text-end upgrade-btn">
                    <!-- Button trigger modal -->
                    <!-- <button type="button" class="btn btn-success d-md-inline-block text-white" data-toggle="modal" data-target="#tambahBursa">
                        Tambah
                    </button> -->
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <div class="card-body profile-card">
                        <center class="mt-4"> <img src="{{ asset('/profil/'.$graduate->user->profil) }}"
                                class="rounded-circle" width="150" />
                            <h4 class="card-title mt-2">{{ $graduate->name }}</h4>
                            <h6 class="card-subtitle">NISN : {{ $graduate->nisn }}</h6>
                            <div class="row justify-content-center">
                                <div class="col-4">
                                    <a href="javascript:void(0)" class="link">
                                        <i class="icon-people" aria-hidden="true"></i>
                                        <span class="font-normal">254</span>
                                    </a></div>
                                <div class="col-4">
                                    <a href="javascript:void(0)" class="link">
                                        <i class="icon-picture" aria-hidden="true"></i>
                                        <span class="font-normal">54</span>
                                    </a></div>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <div class="card-body">
                        <form  method="post" action="{{ route('update.bekerja', $work) }}" class="form-horizontal form-material mx-2">
                        @csrf
                        @method('PATCH')
                            <div class="form-group">
                                <label class="col-md-12 mb-0">Nama Perusahaan/Instansi :</label> 
                                <div class="col-md-12">
                                    <input type="text" name="nma_pru" value="{{ $work->nma_pru }}"
                                        class="form-control ps-0 form-control-line">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Posisi Kerja :</label>
                                <div class="col-md-12">
                                    <input type="text" name="posisi" value="{{ $work->posisi }}"
                                        class="form-control ps-0 form-control-line" name="example-email"
                                        id="example-email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0">Mulai Kerja :</label>
                                <div class="col-md-12">
                                    <input type="date" name="mulai" value="{{ $work->mulai }}"
                                        class="form-control ps-0 form-control-line">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Status Kerja :</label>
                                <div class="col-sm-12 border-bottom">
                                    <select name="status_id" class="form-select shadow-none border-0 ps-0 form-control-line">
                                        @foreach( $statuses as $status )
                                            <option 
                                            value="{{ $status->id }}"
                                            @if ($status->id === $work->status_id)
                                            selected
                                            @endif>
                                            {{ $status->status }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Sesuai bidang keahlian :</label>
                                <div class="form-check form-check-inline">
                                    <label class="col-sm-12" for="sesu_bidkeah">
                                        <input class="mr-2" type="radio" name="sesu_bidkeah" value="ya" id="perjanjian" {{$work->sesu_bidkeah == 'ya'? 'checked' : ''}} >Ya <br>
                                        <input class="mr-2" type="radio" name="sesu_bidkeah" value="tidak" id="perjanjian" {{$work->sesu_bidkeah == 'tidak'? 'checked' : ''}} >Tidak
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Perjanjian Kerja :</label>
                                <div class="form-check form-check-inline">
                                    <label class="col-sm-12" for="perjanjian">
                                        <input class="mr-2" type="radio" name="perjanjian" value="kontrak" id="perjanjian" {{$work->perjanjian == 'kontrak'? 'checked' : ''}} >Kontrak <br>
                                        <input class="mr-2" type="radio" name="perjanjian" value="tetap" id="perjanjian" {{$work->perjanjian == 'tetap'? 'checked' : ''}} >Tetap
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Bidang Usaha :</label>
                                <div class="col-md-12">
                                    <input type="text" name="bidus"  value="{{ $work->bidus }}"
                                        class="form-control ps-0 form-control-line">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Lokasi Kerja :</label>
                                <div class="col-md-12">
                                    <input type="text" name="location"  value="{{ $work->location }}"
                                        class="form-control ps-0 form-control-line">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Penghasilan Perbulan :</label>
                                <div class="col-sm-12 border-bottom">
                                    <select name="income_id" class="form-select shadow-none border-0 ps-0 form-control-line">
                                        @foreach( $incomes as $income )
                                            <option 
                                            value="{{ $income->id }}"
                                            @if ($income->id === $work->income_id)
                                            selected
                                            @endif>
                                            {{ $income->nominal }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 d-flex">
                                    <button type="submit" class="btn btn-success mx-auto mx-md-0 text-white">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        © 2021 Bursa Kerja Khusus by <a href="https://smkn1tebas.sch.id/">smkn1tebas.sch.id</a>
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>


@endsection

  