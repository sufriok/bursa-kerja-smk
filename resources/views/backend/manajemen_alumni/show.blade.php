@extends('backend.layouts.main')

@section('content')
<div class="page-wrapper mt-5 pt-4">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row align-items-center">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="page-title mb-0 p-0">Show Alumni</h3>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Show Alumni</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <div class="text-end upgrade-btn">
                    <!-- Button trigger modal -->
                    <!-- <button type="button" class="btn btn-success d-md-inline-block text-white" data-toggle="modal" data-target="#tambahAlumni">
                        Tambah
                    </button> -->
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-5 col-xlg-5 col-md-5">
                <div class="card">
                    <div class="bg-info text-white card-header">
                       <strong>Data Alumni</strong> 
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Nama</h6>
                            </div>
                            <div class="col-sm-8">
                            <p><strong>:</strong> {{ $graduate->name }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>NISN</h6>
                            </div>
                            <div class="col-sm-8">
                            <p><strong>:</strong> {{ $graduate->nisn }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Tahun Lulus</h6>
                            </div>
                            <div class="col-sm-8">
                            <p><strong>:</strong> {{ $graduate->th_lulus }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Jurusan</h6>
                            </div>
                            <div class="col-sm-8">
                            <p><strong>:</strong> {{ $graduate->department }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Keterangan</h6>
                            </div>
                            <div class="col-sm-8">
                            <p><strong>:</strong> {{ $graduate->keterangan }}</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-7 col-xlg-7 col-md-7">
                
                <div class="card">
                    <div class="bg-info text-white card-header">
                       <strong>Data Keterangan</strong> 
                    </div>
                @if($graduate->keterangan == "kuliah")
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Nama Perguruan Tinggi</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $study->nma_prgu }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Jurusan</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $study->jurusan }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Mulai</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $study->mulai }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Status</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $study->status->status }}</p>
                            </div>
                        </div>

                    </div>
                @elseif($graduate->keterangan == "wirausaha")
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Nama Perusahaan</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $business->nma_pru }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Jenis</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $business->jenis }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Mulai</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $business->mulai }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Status</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $business->status->status }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Pendapatan Perbulan</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $business->income->nominal }}</p>
                            </div>
                        </div>

                    </div>
                @elseif($graduate->keterangan == "bekerja")
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Nama Perusahaan</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $work->nma_pru }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Posisi Kerja</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $work->posisi }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Mulai</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $work->mulai }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Status</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $work->status->status }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Sesuai Bidang Keahlian</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $work->sesu_bidkeah }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Perjanjian</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $work->perjanjian }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Bidang Usaha</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $work->bidus }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Location</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $work->location }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Pendapatan Perbulan</h6>
                            </div>
                            <div class="col-sm-6">
                            <p><strong>:</strong> {{ $work->income->nominal }}</p>
                            </div>
                        </div>

                    </div>
                @else
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                            <p><i>Tidak ada data keterangan dari,
                            Silahkan melengkapi data keterangan</i></p>
                        </div>

                    </div>
                @endif
                </div>

            </div>
            <!-- Column -->
        </div>
        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        © 2021 Bursa Kerja Khusus by <a href="https://smkn1tebas.sch.id/">smkn1tebas.sch.id</a>
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection



