@extends('backend.layouts.main')

@section('content')
<div class="page-wrapper mt-5 pt-4">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row align-items-center">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="page-title mb-0 p-0">Keterangan Wirausaha</h3>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Keterangan Wirausaha</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <div class="text-end upgrade-btn">
                    <!-- Button trigger modal -->
                    <!-- <button type="button" class="btn btn-success d-md-inline-block text-white" data-toggle="modal" data-target="#tambahBursa">
                        Tambah
                    </button> -->
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <!-- <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <div class="card-body profile-card">
                        <center class="mt-4"> <img src=" "
                                class="rounded-circle" width="150" />
                            <h4 class="card-title mt-2">{{ $graduate->name }}</h4>
                            <h6 class="card-subtitle">NISN : {{ $graduate->nisn }}</h6>
                            <div class="row justify-content-center">
                                <div class="col-4">
                                    <a href="javascript:void(0)" class="link">
                                        <i class="icon-people" aria-hidden="true"></i>
                                        <span class="font-normal">254</span>
                                    </a></div>
                                <div class="col-4">
                                    <a href="javascript:void(0)" class="link">
                                        <i class="icon-picture" aria-hidden="true"></i>
                                        <span class="font-normal">54</span>
                                    </a></div>
                            </div>
                        </center>
                    </div>
                </div>
            </div> -->
            <!-- Column -->
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form  method="post" action="{{ route('wirausaha', $graduate->id) }}" class="form-horizontal form-material mx-2">
                        @csrf
                        @method('PATCH')
                            <div class="form-group">
                                <label class="col-md-12 mb-0">Nama Perusahaan</label> 
                                <div class="col-md-12">
                                    <input type="text" name="nma_pru"
                                        class="form-control ps-0 form-control-line"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Jenis Usaha</label>
                                <div class="col-md-12">
                                    <input type="text" name="jenis"
                                        class="form-control ps-0 form-control-line" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0">Mulai Usaha</label>
                                <div class="col-md-12">
                                    <input type="date" name="mulai"
                                        class="form-control ps-0 form-control-line"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Status Wirausaha</label>
                                <div class="col-sm-12 border-bottom">
                                    <select name="status_id" class="form-select shadow-none border-0 ps-0 form-control-line">
                                        <option>--pilih status wirausaha--</option>
                                        @foreach( $statuses as $status )
                                            <option value="{{ $status->id }}">{{ $status->status }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Penghasilan Perbulan</label>
                                <div class="col-sm-12 border-bottom">
                                    <select name="income_id" class="form-select shadow-none border-0 ps-0 form-control-line">
                                        <option>--pilih range penghasilan--</option>
                                        @foreach( $incomes as $income )
                                            <option value="{{ $income->id }}">{{ $income->nominal }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button type="submit" class="btn btn-success text-white">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        © 2021 Bursa Kerja Khusus by <a href="https://smkn1tebas.sch.id/">smkn1tebas.sch.id</a>
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>


@endsection

  