@extends('backend.layouts.main')

@section('content')
<div class="page-wrapper mt-5 pt-4">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
    @if(Session::has('success'))
        <div class="alert alert-info alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif
    @if(Session::has('update'))
        <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('update') }}</strong>
        </div>
    @endif
    @if(Session::has('delete'))
        <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('delete') }}</strong>
        </div>
    @endif
        <div class="row align-items-center">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="page-title mb-0 p-0">Manajemen Alumni</h3>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Manajemen Alumni</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <div class="text-end upgrade-btn">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success d-md-inline-block text-white" data-toggle="modal" data-target="#tambahAlumni">
                        Tambah
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
          <!-- column -->
          <div class="col-sm-12">
              <div class="card">
                  <div class="card-body">
                        <p>
                            <button type="button" class="mt-1 btn btn-primary d-md-inline-block text-white" data-toggle="modal" data-target="#importExcel">
                            Import
                            </button>
                            <a href="{{ route('graduate.export') }}"
                            class="mt-1 btn btn-success d-md-inline-block text-white">Export</a>
                            <a href="{{ asset('/public/file_alumni/Template Alumni.xlsx') }}"
                            class="mt-1 btn btn-warning d-md-inline-block text-white"><i class="fa fa-download"></i> Download Template</a>
                        </p>
                        <small>* Sebelum me-import data alumni, silahkan download template terlebih dahulu</small><br>
                        <small>* Data yang memiliki NISN yang sama dengan data yang sudah ada, secara otomatis tidak di-import oleh sistem</small>
                  </div>
              </div>
          </div>
          
        </div> 
        <div class="row">
            <!-- column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <h4 class="card-title">Basic Table</h4>
                        <h6 class="card-subtitle">Add class <code>.table</code></h6> -->
                        <div class="table-responsive">
                            <table class="table user-table table-bordered" id="example-ok">
                                <thead>
                                    <tr class="text-center">
                                        <th class="border-top-0">No</th>
                                        <th class="border-top-0">Nama</th>
                                        <th class="border-top-0">NISN</th>
                                        <th class="border-top-0">Tahun Lulus</th>
                                        <th class="border-top-0">Keterangan</th>
                                        <th class="border-top-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=1 @endphp
                                    @foreach($graduates as $graduate)
                                    <tr class="text-center">
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $graduate->name }}</td>
                                        <td>{{ $graduate->nisn }}</td>
                                        <td>{{ $graduate->th_lulus }}</td>
                                        <td>{{ $graduate->keterangan }}
                                        @if($graduate->keterangan == "kuliah")
                                        <ul class="navbar-nav float-right pr-1 m-0">
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" v-pre>
                                                <i class="fa fa-edit fa-fw"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                    <a class="dropdown-item" href="{{ route('edit.kuliah', $graduate) }}">
                                                        Kuliah
                                                    </a>
                                                    <a class="dropdown-item" href="{{ route('pilih.wirausaha', $graduate) }}">
                                                        Wirausaha
                                                    </a>
                                                    <a class="dropdown-item" href="{{ route('pilih.bekerja', $graduate) }}">
                                                        Bekerja
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                        @elseif($graduate->keterangan == "wirausaha")
                                        <ul class="navbar-nav float-right pr-1 m-0">
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" v-pre>
                                                <i class="fa fa-edit fa-fw"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                    <a class="dropdown-item" href="{{ route('pilih.kuliah', $graduate) }}">
                                                        Kuliah
                                                    </a>
                                                    <a class="dropdown-item" href="{{ route('edit.wirausaha', $graduate) }}">
                                                        Wirausaha
                                                    </a>
                                                    <a class="dropdown-item" href="{{ route('pilih.bekerja', $graduate) }}">
                                                        Bekerja
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                        @elseif($graduate->keterangan == "bekerja")
                                        <ul class="navbar-nav float-right pr-1 m-0">
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" v-pre>
                                                <i class="fa fa-edit fa-fw"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                    <a class="dropdown-item" href="{{ route('pilih.kuliah', $graduate) }}">
                                                        Kuliah
                                                    </a>
                                                    <a class="dropdown-item" href="{{ route('pilih.wirausaha', $graduate) }}">
                                                        Wirausaha
                                                    </a>
                                                    <a class="dropdown-item" href="{{ route('edit.bekerja', $graduate) }}">
                                                        Bekerja
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                        @else
                                        <ul class="navbar-nav float-right pr-1 m-0">
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" v-pre>
                                                <i class="fa fa-edit fa-fw"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                    <a class="dropdown-item" href="{{ route('pilih.kuliah', $graduate) }}">
                                                        Kuliah
                                                    </a>
                                                    <a class="dropdown-item" href="{{ route('pilih.wirausaha', $graduate) }}">
                                                        Wirausaha
                                                    </a>
                                                    <a class="dropdown-item" href="{{ route('pilih.bekerja', $graduate) }}">
                                                        Bekerja
                                                    </a>
                                                </div>
                                            </li> 
                                        </ul>
                                        @endif
                                        </td>
                                        <td>
                                            <form action="{{ route('manajemen_alumni.destroy', $graduate) }}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <a class="btn btn-info" href="{{ route('manajemen_alumni.show', $graduate) }}"><i class="fa fa-eye fa-fw"></i></a>
                                            <a class="btn btn-success" href="{{ route('manajemen_alumni.edit', $graduate) }}"><i class="fa fa-edit fa-fw"></i></a>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data?')"><i class="fa fa-trash fa-fw"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        © 2021 Bursa Kerja Khusus by <a href="https://smkn1tebas.sch.id/">smkn1tebas.sch.id</a>
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

<!-- Modal Tambah Alumni-->
<div class="modal fade" id="tambahAlumni" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-light text-info">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Alumni Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{ route('manajemen_alumni.store') }}">
        @csrf
        <div class="modal-body">

          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label for="exampleInputNama">Nama</label>
                <input type="text" name="name" class="form-control" required>
                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Tahun Lulus</label>
                <input type="number" name="th_lulus" class="form-control" required>
              </div>
            </div>

            <div class="col-6">
              <div class="form-group">
                <label for="exampleInputNISN">NISN</label>
                <input type="number" name="nisn" class="form-control" required>
              </div>
              <div class="form-group">
                <label for="exampleInputJurusan">Jurusan</label>
                <input type="text" name="department" class="form-control"  required>
              </div>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
          <button type="Submit" class="btn btn-info">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Import Excel -->
<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="{{ route('graduate.import') }}" enctype="multipart/form-data">
					<div class="modal-content">
						<div class="modal-header bg-light text-info">
							<h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
						</div>
						<div class="modal-body">
 
							{{ csrf_field() }}
 
							<label>Pilih file excel</label>
							<div class="form-group">
								<input type="file" name="file" required="required"> 
							</div>
 
						</div>
						<div class="modal-footer">
							<!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
							<button type="submit" class="btn btn-info">Import</button>
						</div>
					</div>
				</form>
			</div>
		</div>

@endsection



