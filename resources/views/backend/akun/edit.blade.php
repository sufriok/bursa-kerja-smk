@extends('backend.layouts.main')

@section('content')
<div class="page-wrapper mt-5 pt-4">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row align-items-center">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="page-title mb-0 p-0">Edit Akun</h3>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Akun</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <div class="text-end upgrade-btn">
                    <!-- Button trigger modal -->
                    <!-- <button type="button" class="btn btn-success d-md-inline-block text-white" data-toggle="modal" data-target="#tambahBursa">
                        Tambah
                    </button> -->
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <div class="bg-info text-white card-header">
                       <strong>Data Akun</strong> 
                    </div>
                    <div class="card-body profile-card">
                        <center class="mt-4"> <img src="{{ asset('/profil/'.Auth::user()->profil) }}"
                                class="rounded-circle" width="150" />
                            <!-- <h4 class="card-title mt-2">{{ $user->name }}</h4>
                            <h6 class="card-subtitle">Gmail : {{ $user->email }}</h6>
                            <div class="row justify-content-center">
                                <div class="col-4">
                                    <a href="javascript:void(0)" class="link">
                                        <i class="icon-people" aria-hidden="true"></i>
                                        <span class="font-normal">254</span>
                                    </a></div>
                                <div class="col-4">
                                    <a href="javascript:void(0)" class="link">
                                        <i class="icon-picture" aria-hidden="true"></i>
                                        <span class="font-normal">54</span>
                                    </a></div>
                            </div>  -->
                            <hr>
                        </center>
                        <form  method="post" action="{{ route('akun.update', $user) }}" class="form-horizontal form-material mx-2">
                        @csrf
                        @method('PATCH')
                            <div class="form-group">
                                <label class="col-md-12 mb-0">Nama</label>
                                <div class="col-md-12">
                                    <input type="text" name="name" value="{{ $user->name }}" required
                                        class="form-control ps-0 form-control-line">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Email</label>
                                <div class="col-md-12">
                                    <input type="email" name="email" value="{{ $user->email }}" required
                                        class="form-control ps-0 form-control-line">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0">Password</label>
                                <div class="col-md-12">
                                    <input type="password" name="password" placeholder="..........."
                                        class="form-control ps-0 form-control-line">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 text-right">
                                    <button type="submit" class="btn btn-success mx-auto mx-md-0 text-white">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <div class="bg-info text-white card-header">
                       <strong>Data Alumni</strong> 
                    </div>
                    @isset($user->graduate->name)
                    <div class="card-body">
                        <form class="form-horizontal form-material mx-2">
                            <div class="form-group">
                                <label class="col-md-12 mb-0">Nama</label>
                                <div class="col-md-12">
                                    <input value="{{ $user->graduate->name }}"
                                        class="form-control ps-0 form-control-line" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">NISN</label>
                                <div class="col-md-12">
                                    <input value="{{ $user->graduate->nisn }}"
                                        class="form-control ps-0 form-control-line" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0">Tahun Lulus</label>
                                <div class="col-md-12">
                                    <input value="{{ $user->graduate->th_lulus }}"
                                        class="form-control ps-0 form-control-line" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0">Jurusan</label>
                                <div class="col-md-12">
                                    <input value="{{ $user->graduate->department }}"
                                        class="form-control ps-0 form-control-line" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 mb-0">Keterangan</label>
                                <div class="col-md-12">
                                    <input value="{{ $user->graduate->keterangan }}"
                                        class="form-control ps-0 form-control-line" readonly>
                                </div>
                            </div>
                        </form>
                        <div class="form-group">
                            <div class="col-sm-12 text-right">
                                <a href="{{ route('hapus.graduate', $user->id) }}" class="btn btn-warning mx-auto mx-md-0 text-white">Hapus</a>
                            </div>
                        </div>
                    </div>
                    @endisset
                    @empty($user->graduate->name)
                    <div class="card-body text-center">
                        <h3><i>Akun ini tidak memiliki data alumni yang dipilih</i></h3>
                    </div>
                    @endempty
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        © 2021 Bursa Kerja Khusus by <a href="https://smkn1tebas.sch.id/">smkn1tebas.sch.id</a>
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>


@endsection

  