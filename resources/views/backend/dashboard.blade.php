@extends('backend.layouts.main')

@section('content')
<div class="page-wrapper mt-5 pt-4">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
    @if(Session::has('success'))
        <div class="alert alert-info alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif
    @if(Session::has('update'))
        <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('update') }}</strong>
        </div>
    @endif
    @if(Session::has('delete'))
        <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('delete') }}</strong>
        </div>
    @endif
        <div class="row align-items-center">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="page-title mb-0 p-0">Dashboard</h3>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!-- <div class="col-md-6 col-4 align-self-center">
                <div class="text-end upgrade-btn">
                    <a href="https://www.wrappixel.com/templates/monsteradmin/"
                        class="btn btn-success d-none d-md-inline-block text-white" target="_blank">Upgrade to
                        Pro</a>
                </div>
            </div> -->
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Sales chart -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Alumni</h4>
                        <div class="text-end">
                            <h1 class="mb-0"><i class="fas fa-address-book fa-2x text-success"></i>&nbsp;&nbsp;{{ $countgraduate }}</h1>
                            <span class="text-muted"></span>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Akun</h4>
                        <div class="text-end">
                            <h1 class="mb-0"><i class="fas fas fa-users fa-2x text-success"></i>&nbsp;&nbsp;{{ $countuser }}</h1>
                            <span class="text-muted"></span>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Bursa Kerja</h4>
                        <div class="text-end">
                            <h1 class="mb-0"><i class="fas fas fa-briefcase fa-2x text-success"></i>&nbsp;&nbsp;{{ $countloker }}</h1>
                            <span class="text-muted"></span>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">MOU</h4>
                        <div class="text-end">
                            <h1 class="mb-0"><i class="fas fa-handshake fa-2x text-success"></i>&nbsp;&nbsp;{{ $countmou }}</h1>
                            <span class="text-muted"></span>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- Column -->
            
            
            <!-- Column -->
            <!-- <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">MOU</h4>
                        <div class="text-end">
                            <h2 class="font-light mb-0"><i class="ti-arrow-up text-info"></i> {{ $countmou }}</h2>
                            <span class="text-muted">Jumlah MOU saat ini</span>
                        </div>
                        <span class="text-info">30%</span>
                        <div class="progress">
                            <div class="progress-bar bg-info" role="progressbar"
                                style="width: 30%; height: 6px;" aria-valuenow="25" aria-valuemin="0"
                                aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- Column -->
        </div>
        
        <!-- ============================================================== -->
        <!-- Table -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
                            <h4 class="card-title">MESSAGES</h4>
                        </div>
                        <div class="table-responsive mt-2">
                            <table class="table stylish-table no-wrap">
                                <thead>
                                    <tr>
                                        <!-- <th class="border-top-0" colspan="2">Assigned</th> -->
                                        <th class="border-top-0">Name</th>
                                        <th class="border-top-0">Email/Subjek</th>
                                        <th class="border-top-0">Pesan</th>
                                        <th class="border-top-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($messages as $message)
                                    <tr> 
                                        <td class="align-middle">
                                            <h6>{{ $message->name }}</h6><small class="text-muted">{{ $message->created_at }}</small>
                                        </td>
                                        <td class="align-middle"><h6>{{ $message->email }}</h6><small class="text-muted">{{ $message->subject }}</small>
                                        </td>
                                        <td class="align-middle">{{ $message->pesan }}</td>
                                        <td class="align-middle">
                                            <form action="{{ route('message.hapus', $message) }}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }} 
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data?')"><i class="fa fa-trash fa-fw"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                    <!-- <tr class="active">
                                        <td><span class="round"><img src="{{ asset('/assets_backend/assets/images/users/2.jpg') }}"
                                                    alt="user" width="50"></span></td>
                                        <td class="align-middle">
                                            <h6>Andrew</h6><small class="text-muted">Project Manager</small>
                                        </td>
                                        <td class="align-middle">Real Homes</td>
                                        <td class="align-middle">$23.9K</td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Table -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Recent blogss -->
        <!-- ============================================================== -->
        <div class="row justify-content-center">
            <!-- Column -->
            <div class="col-lg-12 col-md-12 text-center">
                <hr>
                <h3>POSTINGAN</h3>
                <hr>
            </div>
            <!-- Column -->
        </div>
        <!-- ============================================================== -->
        <!-- Recent blogss -->
        <!-- ============================================================== -->
        <div class="row justify-content-center">
          @foreach($posts as $post)
          
            <div class="col-md-2"><br><br>
              <div class="text-center">
                  <img src="{{ asset('/profil/'.$post->user->profil) }}" class="rounded-circle img-fluid" width="70" alt="">
                  <h5 class="pt-1 mb-0">{{ $post->user->name }}</h5> 
                  <p class="card-text"><small class="text-muted">{{ $post->created_at->diffForHumans()}}</small></p>
              </div>
            </div>
            <div class="col-md-10">
              <a href="#">
                <div class="card border-info mb-3">
                    <!-- <div class="card-header">
                    <img src="{{ asset('images/logobkk.png') }}"  class="rounded-circle" width="50" alt="">  
                    {{ $post->user->name }}
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                    </div> -->
                    <div class="text-right">
                        <form action="{{ route('post.hapus', $post) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }} 
                            <button type="submit" class="m-1 btn btn-danger" onclick="return confirm('Yakin ingin menghapus data?')"><i class="fa fa-trash fa-fw"></i></button>
                        </form>
                    </div>
                    <div class="card-footer text-info"><br>
                      <div class="justify-content-start"> 
                        @if(isset($post->image))
                          <div><img src="{{ asset('postImage/'.$post->image) }}" class="rounded img-fluid" width="450" alt=""></div><br>
                          <div><p class="card-text">{{ $post->content }}</p></div>
                        @else
                          <div><p class="card-text">{{ $post->content }}</p></div>
                        @endif
                      </div>
                    </div>
                    <div class="card-body">
                        <small class="text-muted"><i class="bi bi-people-fill"></i> {{ $post->comments->count() }} Komentar</small>
                        <br>
                        @if(isset($post->comments->last()->content))
                          <a href="#">
                            <div class="logo d-flex align-items-center">
                            <img src="{{ asset('/profil/'.$post->user->profil) }}" class="rounded-circle img-fluid" width="25" alt="">
                            <h6 class="m-2">{{ $post->user->name }}</h6><small class="text-muted"> {{ $post->comments->last()->created_at->diffForHumans() }}</small>
                            </div>
                            <h6 class="m-2"><strong>{{ $post->comments->last()->content }}</strong></h6>
                          </a>
                        @else
                        
                        @endif
                        
                        <!-- <hr> -->

                        <!-- <div class="input-group mb-3">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon1">Send</button>
                            <input type="text" class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
                        </div> -->

                    </div>
                </div>
              </a>
            </div>
          
          @endforeach

          <div class="blog-pagination">
            <ul class="justify-content-center">
              {{ $posts->links() }}
            </ul>
          </div>
          <br>

        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        © 2021 Bursa Kerja Khusus by <a href="https://smkn1tebas.sch.id/">smkn1tebas.sch.id</a>
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection