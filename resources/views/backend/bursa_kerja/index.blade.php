@extends('backend.layouts.main')

@section('content')
<div class="page-wrapper mt-5 pt-4">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
    @if(Session::has('success'))
        <div class="alert alert-info alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif
    @if(Session::has('update'))
        <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('update') }}</strong>
        </div>
    @endif
    @if(Session::has('warning'))
        <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('warning') }}</strong>
        </div>
    @endif
    @if(Session::has('delete'))
        <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('delete') }}</strong>
        </div>
    @endif
        <div class="row align-items-center">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="page-title mb-0 p-0">Bursa Kerja</h3>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Bursa Kerja</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <div class="text-end upgrade-btn">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success d-md-inline-block text-white" data-toggle="modal" data-target="#tambahBursa">
                        Tambah Bursa Kerja
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <!-- column -->
        <div class="col">
            
                
          <div class="row">
            <div class="card col-lg-8">

              <div class="card-body">
                <h6 class="text-center text-muted">Kategori Loker:</h6>
                <hr>
                <div class="row">
                @foreach($categories as $category)
                  <div class="col-sm-6">
                    <!-- <div class="input-group">
                      <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username with two button addons">
                      <button class="btn btn-outline-secondary" type="button">Button</button>
                      <button class="btn btn-outline-secondary" type="button">Button</button>
                    </div> -->
                    <div class="float-left">
                      <form action="{{ route('category.update', $category) }}" method="post">
                        @csrf
                        @method('PATCH')
                        <input type="text" name="kategori" value="{{ $category->kategori}}">
                        <button type="submit" class="btn btn-sm btn-outline-success" onclick="return confirm('Yakin mengubah data?')"><i class="fa fa-edit fa-fw"></i></button>
                      </form>
                    </div>
                    <div class="float-left ml-1">    
                        <form action="{{ route('category.destroy', $category) }}" method="post">
                          @csrf
                          @method('DELETE')
                          <button type="submit" class="btn btn-sm btn-outline-danger" onclick="return confirm('Yakin ingin menghapus data?')"><i class="fa fa-trash fa-fw"></i></button>
                        </form>
                    </div>
                  </div>
                @endforeach
                </div>
              </div>
            
            </div>
            <!-- <div class="col-sm-1"></div> -->
            <div class="card col-lg-4">
              <div class="card-body">
                <form method="post" action="{{ route('category.store') }}">
                  @csrf
                  <div class="form-group">
                    <label for="exampleInputNama"><strong>Kategori Baru</strong></label>
                    <input type="text" name="kategori" class="form-control" id="exampleInputNama" required>
                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                  </div>
                  <button type="Submit" class="float-right btn btn-primary">Tambahkan</button>
                </form>
              </div>
            </div>
          
          
              <!-- <button type="button" class="btn btn-primary d-md-inline-block text-white" data-toggle="modal" data-target="#importExcel">
                Import
              </button>
              <a href="{{ route('graduate.export') }}"
              class="btn btn-success d-md-inline-block text-white">Export</a>
              <a href="#"
              class="btn btn-warning d-md-inline-block text-white">Download Template</a></p>
              <small>* Sebelum me-import data alumni, silahkan download template terlebih dahulu</small> -->
          </div>
            
        </div>
          

        <div class="row">
            <!-- column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <h4 class="card-title">Basic Table</h4>
                        <h6 class="card-subtitle">Add class <code>.table</code></h6> -->
                        <div class="table-responsive">
                            <table class="table user-table table-bordered" id="example-ok">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">#</th>
                                        <th class="border-top-0">Penerimaan</th>
                                        <th class="border-top-0">Judul</th>
                                        <th class="border-top-0">Perusahaan</th>
                                        <th class="border-top-0">kategori</th>
                                        <th class="border-top-0">Brosur</th>
                                        <th class="border-top-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i=1 @endphp
                                    @foreach($lokers as $loker)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $loker->tgl_awal }}<br><i>-sampai-</i><br>{{ $loker->tgl_akhir }}</td>
                                        <td>{{ $loker->title }}</td>
                                        <td>{{ $loker->perusahaan }}</td>
                                        <td>{{ $loker->category->kategori }}</td>
                                        <td><img src="{{ asset('/brosurKerja/'.$loker->brosur) }}" class="img-fluid img-responsive"  width="250" height="120"></td>
                                        <td>
                                            <form action="{{ route('bursa_kerja.destroy', $loker) }}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }} 
                                            <a class="btn btn-info" href="{{ route('bursa_kerja.show', $loker) }}"><i class="fa fa-eye fa-fw"></i></a>
                                            <a class="btn btn-success" href="{{ route('bursa_kerja.edit', $loker) }}"><i class="fa fa-edit fa-fw"></i></a>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data?')"><i class="fa fa-trash fa-fw"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== --> 
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        © 2021 Bursa Kerja Khusus by <a href="https://smkn1tebas.sch.id/">smkn1tebas.sch.id</a>
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

<!-- Modal Tambah Alumni-->
<div class="modal fade" id="tambahBursa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-secondary text-white">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Bursa Kerja Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{ route('bursa_kerja.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">

          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label for="exampleInputNama">Nama</label>
                <input type="text" name="title" class="form-control" id="exampleInputNama" required>
                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Deskripsi</label>
                <textarea name="deskripsi" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputNama">Perusahaan</label>
                <input type="text" name="perusahaan" class="form-control" id="exampleInputNama" required>
                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
              </div>
            </div>

            <div class="col-6">
              <div class="form-group">
                <label for="exampleInputPassword1">Kategori</label>
                  <select name="category_id" class="form-control">
                      <option>--pilih kategori--</option>
                      @foreach( $categories as $category )
                          <option value="{{ $category->id }}">{{ $category->kategori }}</option>
                      @endforeach
                  </select>
              </div>
              <div class="form-group">
                <label for="exampleInputNISN">Brosur</label>
                <input type="file" name="brosur" class="form-control" id="exampleInputNISN" required>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Tanggal Mulai</label>
                <input type="date" name="tgl_awal" class="form-control" id="exampleInputThLulus" required>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Tanggal Tutup</label>
                <input type="date" name="tgl_akhir" class="form-control" id="exampleInputThLulus" required>
              </div>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
          <button type="Submit" class="btn btn-info">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection
