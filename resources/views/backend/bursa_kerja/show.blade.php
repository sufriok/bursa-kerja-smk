@extends('backend.layouts.main')

@section('content')
<div class="page-wrapper mt-5 pt-4">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row align-items-center">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="page-title mb-0 p-0">Show Alumni</h3>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Show Alumni</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <div class="text-end upgrade-btn">
                    <!-- Button trigger modal -->
                    <!-- <button type="button" class="btn btn-success d-md-inline-block text-white" data-toggle="modal" data-target="#tambahAlumni">
                        Tambah
                    </button> -->
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-5 col-xlg-4 col-md-6">
                <div class="card">
                    <div class="bg-info text-white card-header">
                       <strong>Data Bursa Kerja</strong> 
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Judul</h6>
                            </div>
                            <div class="col-sm-8">
                            <p><strong>:</strong> {{ $loker->title }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Deskripsi</h6>
                            </div>
                            <div class="col-sm-8">
                            <p><strong>:</strong> {{ $loker->deskripsi }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Perusahaan</h6>
                            </div>
                            <div class="col-sm-8">
                            <p><strong>:</strong> {{ $loker->perusahaan }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Waktu</h6>
                            </div>
                            <div class="col-sm-8">
                            <p><strong>:</strong> {{ $loker->tgl_awal }} - {{ $loker->tgl_akhir }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                            <h6>Kategori</h6>
                            </div>
                            <div class="col-sm-8">
                            <p><strong>:</strong> {{ $loker->category->kategori }}</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-7 col-xlg-8 col-md-6">
                <div class="card">
                    <div class="bg-info text-white card-header">
                       <strong>Brosur Bursa Kerja</strong> 
                    </div>
                    <div class="card-body profile-card">
                        <center class="mt-4"> 
                        <img src="{{ asset('/brosurKerja/'.$loker->brosur) }}"
                        class="img-fluid" alt="Responsive image"/>
                        </center>
                    </div>

                </div>
                

            </div>
            <!-- Column -->
        </div>
        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        © 2021 Bursa Kerja Khusus by <a href="https://smkn1tebas.sch.id/">smkn1tebas.sch.id</a>
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection



