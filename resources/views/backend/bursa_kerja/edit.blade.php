@extends('backend.layouts.main')

@section('content')
<div class="page-wrapper mt-5 pt-4">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row align-items-center">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="page-title mb-0 p-0">Edit Bursa Kerja</h3>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Bursa Kerja</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <div class="text-end upgrade-btn">
                    <!-- Button trigger modal -->
                    <!-- <button type="button" class="btn btn-success d-md-inline-block text-white" data-toggle="modal" data-target="#tambahBursa">
                        Tambah
                    </button> -->
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="bg-info text-white card-header">
                       <strong>Data Bursa Kerja</strong> 
                    </div>
                    <div class="card-body">
                        <!-- <h4 class="card-title">Basic Table</h4>
                        <h6 class="card-subtitle">Add class <code>.table</code></h6> -->
                        <form method="post" action="{{ route('bursa_kerja.update', $loker->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="row form-body">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="exampleInputNama">Title</label>
                                        <input type="text" name="title" value="{{ $loker->title }}" class="form-control" id="exampleInputNama" aria-describedby="emailHelp" placeholder="">
                                        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputNama">Deskripsi</label>
                                        <textarea name="deskripsi" value="" class="form-control" id="exampleFormControlTextarea1" rows="4">{{ $loker->deskripsi }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputNama">Perusahaan</label>
                                        <input type="text" name="perusahaan" value="{{ $loker->perusahaan }}" class="form-control" id="exampleInputNama" aria-describedby="emailHelp" placeholder="">
                                        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                                </div>

                                <div class="col-6">
                                    
                                    <div class="form-group">
                                        <label>Brosur</label>
                                        <input type="file" name="brosur" value="{{ $loker->brosur }}" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputNama">Tanggal Awal</label>
                                        <input type="date" name="tgl_awal" value="{{ $loker->tgl_awal }}" class="form-control">
                                        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputNama">Tanggal Akhir</label>
                                        <input type="date" name="tgl_akhir" value="{{ old('$loker->tgl_akhir') }}" class="form-control">
                                        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Kategori</label>
                                        <select name="category_id" class="form-control">
                                            <option>--pilih kategori--</option>
                                            @foreach( $categories as $category )
                                                <option value="{{ $category->id }}">{{ $category->kategori }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="form-actions">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <!-- <button type="reset" class="btn btn-dark">Reset</button> -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        © 2021 Bursa Kerja Khusus by <a href="https://smkn1tebas.sch.id/">smkn1tebas.sch.id</a>
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>


@endsection