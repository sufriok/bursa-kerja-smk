@extends('frontend.layouts.main')

@section('content')
<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <!-- <ol>
          <li><a href="/">Beranda</a></li>
          <li>Profil</li>
        </ol> -->
        
        <!-- <div class="row">
        <div class="col"><h5>Keterangan Bekerja</h5></div>
        <div class="col mx-0">
        <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-outline-light" style="float: right;">Kembali</a>
        </div>
        </div> -->

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">

      <div class="container" data-aos="fade-up">
        <div class="row">
          <div class="col mx-0">
          <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-info text-white" style="float: right;">Kembali</a>
          </div>
        </div>
        <br>

        <div class="row">
            <!-- column -->
            <div class="col-sm-3">
                <div class="card text-info border-info mb-3">
                    <div class="card-header">PENTING</div>
                    <div class="card-body">
                        <!-- <h5 class="card-title">Info card title</h5> -->
                        <p class="card-text">Isi form keterangan sesuai keterang anda saat ini, setelah menjadi alumni.</p>
                        <!-- <p class="card-text"></p> -->
                    </div>
                </div>
            </div>
            <!-- column -->
            <div class="col-sm-9">
                <div class="card border-dark mb-3">
                    <div class="card-header text-center"><strong>Form Bekerja</strong></div>
                    <div class="card-body">
                        <!-- <h4 class="card-title">Basic Table</h4>
                        <h6 class="card-subtitle">Add class <code>.table</code></h6> -->
                        <form action="{{ route('profil.bekerja', $graduate->id) }}" method="post" class="form-edit">
                         @csrf
                         @method('PATCH')
                          <div class="row gy-4">

                            <div class="col-md-6">
                              <h6>Nama Perusahaan/Instansi</h6>
                              <input type="text" name="nma_pru" class="form-control" required>
                            </div>

                            <div class="col-md-6 ">
                              <h6>Posisi kerja</h6>
                              <input type="text" class="form-control" name="posisi" required>
                            </div>

                            <div class="col-md-6">
                              <h6>Mulai Kerja</h6>
                              <input type="date" class="form-control" name="mulai" required>
                            </div>

                            <div class="col-md-6">
                              <h6>Status</h6>
                              <select name="status_id" class="form-control">
                                  <option>--pilih status kerja--</option>
                                  @foreach( $statuses as $status )
                                      <option value="{{ $status->id }}">{{ $status->status }}</option>
                                  @endforeach
                              </select>
                            </div>

                            <div class="col-md-6">
                              <h6>Pekerjaan sesuai dengan bidang keahlian</h6>
                              <div class="form-check form-check-inline">
                                  <label for="sesu_bidkeah">
                                      <input class="mr-2" type="radio" name="sesu_bidkeah" value="ya" id="sesu_bidkeah">Ya <br>
                                      <input class="mr-2" type="radio" name="sesu_bidkeah" value="tidak" id="sesu_bidkeah">Tidak
                                  </label>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <h6>Perjanjian Kerja</h6>
                              <div class="form-check form-check-inline">
                                  <label for="perjanjian">
                                      <input class="mr-2" type="radio" name="perjanjian" value="kontrak" id="perjanjian">Kontrak <br>
                                      <input class="mr-2" type="radio" name="perjanjian" value="tetap" id="perjanjian">Tetap
                                  </label>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <h6>Bidang Usaha</h6>
                              <input type="text" class="form-control" name="bidus" required>
                            </div>

                            <div class="col-md-6">
                              <h6>Lokasi Kerja</h6>
                              <input type="text" class="form-control" name="location" required>
                            </div>

                            <div class="col-md-6">
                              <h6>Penghasilan Perbulan</h6>
                              <select name="income_id" class="form-control">
                                <option>--pilih range penghasilan--</option>
                                @foreach( $incomes as $income )
                                    <option value="{{ $income->id }}">{{ $income->nominal }}</option>
                                @endforeach
                              </select>
                            </div>

                            <div class="col-md-12 text-center">
                              <button type="submit">Simpan Data</button>
                            </div>

                          </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>

      </div>

    </section><!-- End Contact Section -->
  </main><!-- End #main -->
@endsection

