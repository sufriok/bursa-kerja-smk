@extends('frontend.layouts.main')

@section('content')

<main id="main">

  <!-- ======= Breadcrumbs ======= -->
  <section class="breadcrumbs">
    <div class="container">

      <ol>
        <li><a href="/">Beranda</a></li>
        <li>Detail Loker</li>
      </ol>
      <h5>Detail Lowongan Kerja</h5>

    </div>
  </section><!-- End Breadcrumbs -->

  <!-- ======= Portfolio Details Section ======= -->
  <section id="portfolio-details" class="portfolio-details">
    <div class="container">

      <div class="row gy-4">

        <div class="col-lg-8">
          <div class="portfolio-details-slider swiper-container">
            <div class="swiper-wrapper align-items-center">

              <div class="swiper-slide">
                <img src="{{ asset('/brosurKerja/'.$loker->brosur) }}" alt="">
              </div>

              <!-- <div class="swiper-slide">
                <img src="assets/img/portfolio/portfolio-2.jpg" alt="">
              </div>

              <div class="swiper-slide">
                <img src="assets/img/portfolio/portfolio-3.jpg" alt="">
              </div> -->

            </div>
            <div class="swiper-pagination"></div>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="portfolio-info">
            <h3>Informasi Loker</h3>
            <ul>
              <li><strong>Kategori</strong>: {{ $loker->category->kategori }}</li>
              <li><strong>Client</strong>: ASU Company</li>
              <li><strong>Pendaftaran</strong>: {{ $loker->tgl_akhir }} - {{ $loker->tgl_akhir }}</li>
              <li><strong>Project URL</strong>: <a href="#">www.example.com</a></li>
            </ul>
          </div>
          <div class="portfolio-description">
            <h2>{{ $loker->title}}</h2>
            <p>{{ $loker->deskripsi}}</p>
          </div>
        </div>

      </div>

    </div>
  </section><!-- End Portfolio Details Section -->

</main><!-- End #main -->

@endsection