@extends('frontend.layouts.main')

@section('content')
<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <!-- <ol>
          <li><a href="/">Beranda</a></li>
          <li>Profil</li>
        </ol>
        <h5>Profil Alumni</h5> -->

      </div>
    </section><!-- End Breadcrumbs -->
<br>
    <div class="container">
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <div class="d-flex align-items-center">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
            <div>
            <strong>Success! </strong>{{ Session::get('success') }}
            </div>
            
          </div>
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
      @endif
      @if(Session::has('update'))
        <div class="alert alert-primary alert-dismissible fade show" role="alert">
          <div class="d-flex align-items-center">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Update:"><use xlink:href="#info-fill"/></svg>
            <div>
            <strong>Update! </strong>{{ Session::get('update') }}
            </div>
            
          </div>
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
      @endif
      @if(Session::has('delete'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <div class="d-flex align-items-center">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
            <div>
            <strong>Delete! </strong>{{ Session::get('delete') }}
            </div>
            
          </div>
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
      @endif
    </div>

    <div class="container" data-aos="fade-up">

      <div class="row">
        <div class="col-md-4">
          <div class="card text-center">
            <div class="card-header">
              Profil Akun
            </div>
            <div class="card-body">
              <!-- <h5 class="card-title">Special title treatment</h5>
              <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a> -->
              <img src="{{ asset('/profil/'.Auth::user()->profil) }}"
                          class="rounded-circle" width="150" />
                
              <h3>{{ auth()->user()->name }}</h3>
              <p>Email: {{ auth()->user()->email }}<br>
              <!-- Angkatan: {{ auth()->user()->th_lulus }} <br>
              Keterangan: 
              @if($graduate->keterangan == "kuliah")
              <select name="forma" onchange="location = this.value;">
                <option value="#">Kuliah</option>
                <option value="{{ route('ket.bekerja', $graduate) }}">Bekerja</option>
                <option value="{{ route('ket.wirausaha', $graduate) }}">Wirausaha</option>
              </select>
              @elseif($graduate->keterangan == "bekerja")
              <select name="forma" onchange="location = this.value;">
                <option value="#">Bekerja</option>
                <option value="{{ route('ket.kuliah', $graduate) }}">Kuliah</option>
                <option value="{{ route('ket.wirausaha', $graduate) }}">Wirausaha</option>
              </select>
              @elseif($graduate->keterangan == "wirausaha")
              <select name="forma" onchange="location = this.value;">
                <option value="#">Wirausaha</option>
                <option value="{{ route('ket.kuliah', $graduate) }}">Kuliah</option>
                <option value="{{ route('ket.bekerja', $graduate) }}">Bekerja</option>
              </select>
              @else
              <select name="forma" onchange="location = this.value;">
                <option value="">Tidak ada</option><hr>
                <option value="{{ route('ket.kuliah', $graduate) }}">Kuliah</option>
                <option value="{{ route('ket.bekerja', $graduate) }}">Bekerja</option>
                <option value="{{ route('ket.wirausaha', $graduate) }}">Wirausaha</option>
              </select>
              @endif -->
              </p>

              <!-- Button trigger modal -->
              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editAkun">
                Edit Akun
              </button>
            </div>
            <div class="card-footer text-muted">
              
            </div>
          </div>
        </div>

        <div class="col-md-8">
          <div class="card">
            <div class="card-header">
              Profil Alumni
            </div>
            <div class="card-body">
              <!-- <h5 class="card-title">Special title treatment</h5>
              <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a> -->
              <form action="forms/contact.php" method="post" class="form-edit">
                <div class="row gy-4">

                  <div class="col-md-6">
                    <h6>Nama :</h6>
                    <input class="form-control" value="{{ $graduate->name }}" readonly>
                  </div>

                  <div class="col-md-6 ">
                    <h6>NISN :</h6>
                    <input class="form-control" value="{{ $graduate->nisn }}" readonly>
                  </div>

                  <div class="col-md-6 ">
                    <h6>Tahun Lulus :</h6>
                    <input class="form-control" value="{{ $graduate->nisn }}" readonly>
                  </div>

                  <div class="col-md-6 ">
                    <h6>Jurusan :</h6>
                    <input class="form-control" value="{{ $graduate->department }}" readonly>
                  </div>

                  <!-- <div class="col-md-12 text-center">
                    <h6>Keterangan :</h6> -->
                    <!-- <input class="form-control" value="{{ $graduate->keterangan }}" readonly> -->
                    <!-- <select class="btn btn-outline-primary" name="forma" onchange="location = this.value;">
                        <option value=""
                        @if ($graduate->keterangan === "belum ada") selected @endif>Update Keterangan</option>
                        <option value="{{ route('ket.kuliah', $graduate) }}" 
                        @if ($graduate->keterangan === "kuliah") selected @endif>Kuliah</option>
                        <option value="{{ route('ket.bekerja', $graduate) }}"
                        @if ($graduate->keterangan === "bekerja") selected @endif>Bekerja</option>
                        <option value="{{ route('ket.wirausaha', $graduate) }}"
                        @if ($graduate->keterangan === "wirausaha") selected @endif>Wirausaha</option>
                        </select>
                  </div> -->

                </div>
              </form>

              <hr>

              @if($graduate->keterangan == "kuliah")
                <div class="row gy-4">
                  <div class="form-edit">
                    <div class="col-md-12 ">
                      <center>
                      <h5>Keterangan Kuliah :</h5><br>
                      
                      </div>
                      </center>
                      <div class="col-md-12">
                        <form action="{{ route('updateProfil.kuliah', $study) }}" method="post" class="form-edit">
                          @csrf
                          @method('PATCH')
                          <div class="row gy-4"> 

                            <div class="col-md-6">
                              <h6>Nama Perguruan Tinggi :</h6>
                              <input type="text" name="nma_prgu" class="form-control" value="{{ $study->nma_prgu }}" required>
                            </div>

                            <div class="col-md-6 ">
                              <h6>Jurusan Kuliah :</h6>
                              <input type="text" class="form-control" name="jurusan" value="{{ $study->jurusan }}" required>
                            </div>

                            <div class="col-md-6">
                              <h6>Mulai Kuliah :</h6>
                              <input type="date" class="form-control" name="mulai" value="{{ $study->mulai }}" required>
                            </div>

                            <div class="col-md-6">
                              <h6>Status Kuliah :</h6>
                              <select name="status_id" class="form-control">
                                  @foreach( $statuses as $status )
                                      <option 
                                      value="{{ $status->id }}"
                                      @if ($status->id === $study->status_id)
                                      selected
                                      @endif>
                                      {{ $status->status }}</option>
                                  @endforeach
                              </select>
                            </div>

                            <div class="col-md-12 text-center">
                              <button type="submit" class="btn btn-primary">Update Data</button>
                            </div>

                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              @elseif($graduate->keterangan == "wirausaha")
                <div class="row gy-4">
                  <div class="form-edit">
                    <div class="col-md-12 ">
                      <center>
                      <h5>Keterangan Wirausaha :</h5><br>
                      
                      </div>
                      </center>
                      <div class="col-md-12">
                        <form action="{{ route('updateProfil.wirausaha', $business) }}" method="post" class="form-edit">
                          @csrf
                          @method('PATCH')
                          <div class="row gy-4">

                            <div class="col-md-6">
                              <h6>Nama Perusahaan :</h6>
                              <input type="text" name="nma_pru" class="form-control" value="{{ $business->nma_pru }}" required>
                            </div>

                            <div class="col-md-6 ">
                              <h6>Jenis Usaha :</h6>
                              <input type="text" class="form-control" name="jenis" value="{{ $business->jenis }}" required>
                            </div>

                            <div class="col-md-6">
                              <h6>Mulai Usaha :</h6>
                              <input type="date" class="form-control" name="mulai" value="{{ $business->mulai }}" required>
                            </div>

                            <div class="col-md-6">
                              <h6>Status Usaha :</h6>
                              <select name="status_id" class="form-control">
                                  @foreach( $statuses as $status )
                                      <option 
                                      value="{{ $status->id }}"
                                      @if ($status->id === $business->status_id)
                                      selected
                                      @endif>
                                      {{ $status->status }}</option>
                                  @endforeach
                              </select>
                            </div>

                            <div class="col-md-6">
                              <h6>Penghasilan Perbulan :</h6>
                                <select name="income_id" class="form-control">
                                    @foreach( $incomes as $income )
                                        <option 
                                        value="{{ $income->id }}"
                                        @if ($income->id === $business->income_id)
                                        selected
                                        @endif>
                                        {{ $income->nominal }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-12 text-center">
                              <button type="submit" class="btn btn-primary">Update Data</button>
                            </div>

                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              @elseif($graduate->keterangan == "bekerja")
                <div class="row gy-4">
                  <div class="form-edit">
                    <div class="col-md-12 ">
                      <center>
                      <h5>Keterangan Bekerja :</h5><br>
                      
                      </div>
                      </center>
                      <div class="col-md-12">
                        <form action="{{ route('updateProfil.bekerja', $work) }}" method="post" class="form-edit">
                          @csrf
                          @method('PATCH')
                          <div class="row gy-4">

                            <div class="col-md-12">
                              <h6>Nama Perusahaan :</h6>
                              <input type="text" name="nma_pru" class="form-control" value="{{ $work->nma_pru }}" required>
                            </div>

                            <div class="col-md-6 ">
                              <h6>Jenis Usaha :</h6>
                              <input type="text" class="form-control" name="posisi" value="{{ $work->posisi }}" required>
                            </div>

                            <div class="col-md-6">
                              <h6>Mulai Usaha :</h6>
                              <input type="date" class="form-control" name="mulai" value="{{ $work->mulai }}" required>
                            </div>
                            
                            <div class="col-md-6">
                              <h6>Status Bekerja :</h6>
                              <select name="status_id" class="form-control">
                                  @foreach( $statuses as $status )
                                      <option 
                                      value="{{ $status->id }}"
                                      @if ($status->id === $work->status_id)
                                      selected
                                      @endif>
                                      {{ $status->status }}</option>
                                  @endforeach
                              </select>
                            </div>

                            <div class="col-md-6">
                              <h6>Sesuai Bidang Keahlian :</h6>
                              <div class="form-check form-check-inline">
                                  <label class="col-sm-12" for="sesu_bidkeah">
                                      <input class="mr-2" type="radio" name="sesu_bidkeah" value="ya" id="perjanjian" {{$work->sesu_bidkeah == 'ya'? 'checked' : ''}} >Ya <br>
                                      <input class="mr-2" type="radio" name="sesu_bidkeah" value="tidak" id="perjanjian" {{$work->sesu_bidkeah == 'tidak'? 'checked' : ''}} >Tidak
                                  </label>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <h6>Perjanjian Kerja :</h6>
                              <div class="form-check form-check-inline">
                                  <label class="col-sm-12" for="perjanjian">
                                      <input class="mr-2" type="radio" name="perjanjian" value="kontrak" id="perjanjian" {{$work->perjanjian == 'kontrak'? 'checked' : ''}} >Kontrak <br>
                                      <input class="mr-2" type="radio" name="perjanjian" value="tetap" id="perjanjian" {{$work->perjanjian == 'tetap'? 'checked' : ''}} >Tetap
                                  </label>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <h6>Bidang Usaha :</h6>
                              <input type="text" class="form-control" name="bidus" value="{{ $work->bidus }}" required>
                            </div>

                            <div class="col-md-6">
                              <h6>Lokasi Kerja :</h6>
                              <input type="text" class="form-control" name="location" value="{{ $work->location }}" required>
                            </div>

                            <div class="col-md-6">
                              <h6>Penghasilan Perbulan :</h6>
                                <select name="income_id" class="form-control">
                                    @foreach( $incomes as $income )
                                        <option 
                                        value="{{ $income->id }}"
                                        @if ($income->id === $work->income_id)
                                        selected
                                        @endif>
                                        {{ $income->nominal }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-12 text-center">
                              <button type="submit" class="btn btn-primary">Update Data</button>
                            </div>

                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              @else
                <div class="row gy-4">
                  <div class="form-edit">
                    <div class="col-md-12 ">
                      <center>
                      <p><i>Tidak ada data keterangan anda,
                        Silahkan melengkapi data keterangan anda</i></p>
                      
                      </div>
                      </center>
                      <div class="col-md-12 text-center">
                        <select class="btn btn-outline-primary" name="forma" onchange="location = this.value;">
                        <option value="">Pilih Keterangan</option>
                        <option value="{{ route('ket.kuliah', $graduate) }}">Kuliah</option>
                        <option value="{{ route('ket.bekerja', $graduate) }}">Bekerja</option>
                        <option value="{{ route('ket.wirausaha', $graduate) }}">Wirausaha</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              @endif

            </div>
            <div class="card-footer text-muted">
              
            </div>
          </div>
        </div>
      </div>
      

      <br>

      

    </div>
    <hr>
    <div class="container">
      @foreach($posts as $post)
        <div class="row"><br><br>
          <div class="col-md-2 text-center">
              <img src="{{ asset('/profil/'.$post->user->profil) }}"  class="rounded-circle" width="70" alt="">
              <h5 class="pt-1 mb-0">{{ $post->user->name }}</h5> 
              <p class="card-text"><small class="text-muted">{{ $post->created_at->diffForHumans()}}</small></p>
          </div>
          <div class="col-md-10">
            <a href="{{ route('chatroom.show', $post->id) }}">
              <div class="card border-info mb-3">
                  <!-- <div class="card-header">
                  <img src="{{ asset('images/logobkk.png') }}"  class="rounded-circle" width="50" alt="">  
                  {{ $post->user->name }}
                  <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                  </div> -->
                  <div class="text-end">
                      <form action="{{ route('chatroom.destroy', $post) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }} 
                          <button type="submit" class="m-1 btn btn-danger" onclick="return confirm('Yakin ingin menghapus data?')">hapus</button>
                      </form>
                  </div>
                  <div class="card-footer text-info"><br>
                    <div class="justify-content-start"> 
                      @if(isset($post->image))
                        <div><img src="{{ asset('postImage/'.$post->image) }}" class="rounded img-fluid" width="450" alt=""></div><br>
                        <div><p class="card-text">{{ $post->content }}</p></div>
                      @else
                        <div><p class="card-text">{{ $post->content }}</p></div>
                      @endif
                    </div>
                  </div>
                  <div class="card-body">
                      <small class="text-muted"><i class="bi bi-people-fill"></i> {{ $post->comments->count() }} Komentar</small>
                      <br>
                      <a href="{{ route('chatroom.show', $post->id) }}">
                        
                        @if ( !empty($post->comments->last()->content) )
                        <div class="logo d-flex align-items-center">
                          <img src="{{ asset('/profil/'.$post->user->profil) }}" class="rounded-circle" width="25" alt="">
                          <h6 class="m-2">{{ $post->user->name }}</h6><small class="text-muted"> {{ $post->comments->last()->created_at->diffForHumans() }}</small>
                        </div>
                          <h6 class="m-2"><strong>{{ $post->comments->last()->content }}</strong></h6>
                        @else
                          
                        @endif
                        
                      </a>

                      <!-- <div class="search">
                          <div class="breadcrumbs-item breadcrumbs-form">
                            <form action="{{ route('comment.store', $post) }}" method="post">
                              @csrf
                              @method('PATCH')
                              <input type="text" name="content" value="Komentar">
                              <button type="submit"><i class="bi bi-cursor-fill"></i></button>
                            </form>
                          </div>
                      </div> -->

                  </div>
              </div>
            </a>
          </div>
        </div>
      @endforeach
    </div>

</main><!-- End #main -->

<!-- Modal -->
<div class="modal fade" id="editAkun" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Edit Akun</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{ route('updateProfil', auth()->user()->id) }}" method="post" class="form-edit" enctype="multipart/form-data">
          @csrf
          @method('PATCH')
          <div class="row gy-4">

            <div class="col-md-6">
              <h6>Foto Profil :</h6>
              <input type="file" name="profil" class="form-control">
            </div>

            <div class="col-md-6">
              <h6>Nama :</h6>
              <input type="text" name="name" class="form-control" value="{{ auth()->user()->name }}" required>
            </div>

            <div class="col-md-6 ">
              <h6>Email :</h6>
              <input type="email" class="form-control" name="email" value="{{ auth()->user()->email }}" required>
            </div>

            <div class="col-md-6">
              <h6>Password :</h6>
              <input type="password" class="form-control" name="password" placeholder="************">
            </div>

          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
        </form>
    </div>
  </div>
</div>
@endsection

