@extends('frontend.layouts.main')

@section('content')
<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <!-- <ol>
          <li><a href="/">Beranda</a></li>
          <li>Register</li>
        </ol>
        <h5>Register Alumni</h5> -->

      </div>
    </section><!-- End Breadcrumbs -->
<br>
    <div class="container">
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <div class="d-flex align-items-center">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
            <div>
            <strong>Success! </strong>{{ Session::get('success') }}
            </div>
            
          </div>
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
      @endif
      @if(Session::has('update'))
        <div class="alert alert-primary alert-dismissible fade show" role="alert">
          <div class="d-flex align-items-center">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Update:"><use xlink:href="#info-fill"/></svg>
            <div>
            <strong>Update! </strong>{{ Session::get('update') }}
            </div>
            
          </div>
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
      @endif
      @if(Session::has('delete'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <div class="d-flex align-items-center">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
            <div>
            <strong>Delete! </strong>{{ Session::get('delete') }}
            </div>
            
          </div>
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
      @endif
    </div>
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">

      <div class="container" data-aos="fade-up">

        <!-- <header class="section-header">
          <h2>Contact</h2>
          <p>Contact Us</p>
        </header> -->

        <!-- <div class="row gy-4 m-auto">

          <div class="col-lg-12 form-edit">
    
              <div class="row gy-4">

                <div class="col-md-12 text-center">
                  <h3>Pilih data alumni anda</h3>
                </div>

                <div class="col-md-12">
                  <div class="table-responsive">
                      <table class="table user-table table-bordered" id="example-ok">
                          <thead>
                              <tr>
                                  <th class="border-top-0">#</th>
                                  <th class="border-top-0">Nama</th>
                                  <th class="border-top-0">NISN</th>
                                  <th class="border-top-0">Tahun Lulus</th>
                                  <th class="border-top-0">Keterangan</th>
                                  <th class="border-top-0">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                          @php $i=1 @endphp
                              @foreach($graduates as $graduate)
                              <tr>
                                  <td>{{ $i++ }}</td>
                                  <td>{{ $graduate->name }}</td>
                                  <td>{{ $graduate->nisn }}</td>
                                  <td>{{ $graduate->th_lulus }}</td>
                                  <td>{{ $graduate->keterangan }}</td>
                                  <td>
                                      <a class="btn btn-success" href="{{ route('manajemen_alumni.edit', $graduate) }}"><i class="fa fa-edit fa-fw"></i> Pilih</a>
                                  </td>
                              </tr>
                              @endforeach
                          </tbody>
                      </table>
                  </div>
                </div>

              </div>

          </div>

          

        </div> -->
        
        <div class="row">
            <!-- column -->
            <div class="col-sm-3">
                <div class="card text-info border-info mb-3">
                    <div class="card-header">PENTING</div>
                    <div class="card-body">
                        <!-- <h5 class="card-title">Info card title</h5> -->
                        <p class="card-text">Pilihlah sesuai data alumni anda pada daftar alumni disamping.</p>
                        <p class="card-text">Apabila data alumni anda tidak ada pada daftar alumni disamping silahkan tinggalkan pesan.</p>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#pesan">
                            Pesan
                        </button>
                    </div>
                </div>
            </div>
            <!-- column -->
            <div class="col-sm-9">
                <div class="card border-dark mb-3">
                    <div class="card-header text-center"><strong>Daftar Alumni</strong></div>
                    <div class="card-body">
                        <!-- <h4 class="card-title">Basic Table</h4>
                        <h6 class="card-subtitle">Add class <code>.table</code></h6> -->
                        <div class="table-responsive">
                            <table class="table user-table table-bordered" id="example-ok">
                                <thead>
                                    <tr class="text-center">
                                        <th class="border">No</th>
                                        <th class="border">Nama</th>
                                        <th class="border">NISN</th>
                                        <th class="border">Tahun Lulus</th>
                                        <!-- <th class="border">Keterangan</th> -->
                                        <th class="border">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=1 @endphp
                                    @foreach($graduates as $graduate)
                                    <tr class="text-center">
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $graduate->name }}</td>
                                        <td>{{ $graduate->nisn }}</td>
                                        <td>{{ $graduate->th_lulus }}</td>
                                        <!-- <td>{{ $graduate->keterangan }}</td> --> 
                                        <td>
                                            <a class="btn text-warning border-warning" href="{{ route('pilihGraduate', $graduate) }}"><i class="fa fa-edit fa-fw"></i> Pilih Data</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      </div>

    </section><!-- End Contact Section -->
  </main><!-- End #main -->
@endsection

<!-- Modal -->
<div class="modal fade" id="pesan" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Message</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{ route('kirim.message') }}" method="post" class="form-edit">
          @csrf
          @method('PATCH')
          <div class="row gy-4">

            <div class="col-md-6">
              <h6>Name :</h6>
              <input type="text" name="name" class="form-control" required>
            </div>

            <div class="col-md-6">
              <h6>Email :</h6>
              <input type="email" name="email" class="form-control" required>
            </div>

            <div class="col-md-12">
              <h6>Subjek :</h6>
              <input type="text" name="subject" class="form-control" required>
            </div>

            <div class="col-md-12">
              <h6>Pesan :</h6>
              <textarea name="pesan" class="form-control" id="exampleFormControlTextarea1" rows="5" required></textarea>
            </div>

          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Kirim</button>
      </div>
        </form>
    </div>
  </div>
</div>


