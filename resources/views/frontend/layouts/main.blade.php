<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->

  <title>Bursa Kerja Khusus (BKK) SMKN 1 Tebas</title>
  <meta content="" name="description">

  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('images/'.$website->header_logo) }}" rel="icon">
  <link href="{{ asset('images/'.$website->header_logo) }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets_frontend/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets_frontend/assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
  <link href="{{ asset('assets_frontend/assets/vendor/aos/aos.css') }}" rel="stylesheet">
  <link href="{{ asset('assets_frontend/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('assets_frontend/assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets_frontend/assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('assets_frontend/assets/css/style.css') }}" rel="stylesheet">
  <!-- Custom CSS -->
  <!-- <link href="{{ asset('/assets_backend/css/style.min.css') }}" rel="stylesheet"> -->
  
  
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.3/datatables.min.css"/>
  <!-- <link rel="stylesheet" type="text/css" href="{{ asset('/assets_backend/vendor/datatables/datatables.min.css') }}"/> -->
  <!-- <link rel="stylesheet" href="{{ asset('/assets_backend/bootstrap452/bootstrap.min.css') }}"> -->

  <!-- =======================================================
  * Template Name: FlexStart - v1.2.0
  * Template URL: https://bootstrapmade.com/flexstart-bootstrap-startup-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
  <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
  </symbol>
  <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
    <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
  </symbol>
  <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
    <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
  </symbol>
</svg>

  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <a href="index.html" class="logo d-flex align-items-center">
        <img src="{{ asset('images/'.$website->header_logo) }}" alt="">
        <h5 class="mt-2"><strong>{{ $website->header_title }}</strong></h5>
      </a>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto" href="/.#hero">Beranda</a></li>
          <!-- <li><a class="nav-link scrollto" href="#services">Services</a></li> -->
          <li><a class="nav-link scrollto" href="/.#about">Tentang</a></li>
          <li><a class="nav-link scrollto" href="/.#recent-blog-posts">Loker</a></li>
          <!-- <li><a class="nav-link scrollto" href="/.#contact">Kontak</a></li> -->
          <li><a href="{{ route('show.all') }}">Cari</a></li>
          @guest
              <li><a class="nav-link" href="{{ route('login') }}">Login</a></li>
              @if (Route::has('register'))
                  <li><a class="nav-link" href="{{ route('register') }}">Register</a></li>
              @endif
          @else 
              @if(Auth::user()->status == "2")
              <li><a href="{{ route('chatroom.index') }}">{{ __('Postingan') }}</a></li>
              <li class="dropdown"><a href="#"><span>Home</span> <i class="bi bi-chevron-down"></i></a>
                <ul>
                  <li><a href="{{ url('/home') }}">Profil</a></li>
                  <li><a class="dropdown-item" href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                          {{ __('Logout') }}
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                </ul>
              </li>
              @elseif(Auth::user()->status == "1")
              <li><a href="{{ url('/home') }}"><i class="bi bi-door-open-fill" style="font-size: 1.5em;"></i></a></li>
              @endif
          @endguest

          <!-- <li><a class="getstarted scrollto" href="#about">Get Started</a></li> -->
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

    @yield('content')
  
  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <!-- <div class="col-lg-12 text-center">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
          </div>
          <div class="col-lg-6">
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div> -->
        </div>
      </div>
    </div>

    <div class="footer-top">
      <div class="container">
        <div class="row gy-4">
          <div class="col-lg-5 col-md-12 footer-info">
            <a href="/" class="logo d-flex align-items-center">
              <img src="{{ asset('images/logobkk.png') }}" alt="">
              <span>BKK SMKN 1 Tebas</span>
            </a>
            <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus.</p>
            <div class="social-links mt-3">
              <a href="{{ $website->footer_twitter }}" class="twitter"><i class="bi bi-twitter"></i></a>
              <a href="{{ $website->footer_facebook }}" class="facebook"><i class="bi bi-facebook"></i></a>
              <a href="{{ $website->footer_instagram }}" class="instagram"><i class="bi bi-instagram bx bxl-instagram"></i></a>
              <a href="#" class="linkedin"><i class="bi bi-linkedin bx bxl-linkedin"></i></a>
              <a href="{{ $website->footer_youtube }}" class="youtube"><i class="bi bi-youtube"></i></a>
            </div>
          </div>

          <div class="col-lg-2 col-6 footer-links">
            <h4>Menu Utama</h4>
            <ul>
              <li><i class="bi bi-chevron-right"></i> <a href="/.#hero">Beranda</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="/.#about">Tentang</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="/.#recent-blog-posts">Loker</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="/.#contact">Kontak</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="{{ route('show.all') }}">Cari Loker</a></li>
            </ul>
          </div>

          <div class="col-lg-2 col-6 footer-links">
            <h4>Kategori Loker</h4>
            <ul>

              @foreach( $categories as $category )
              <li><i class="bi bi-chevron-right"></i> <a href="{{ route('category.loker', $category) }}">{{ $category->kategori }} <span>({{ $category->loker->count() }})</span></a></li>
              @endforeach

              <!-- <li><i class="bi bi-chevron-right"></i> <a href="#">Web Design</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Graphic Design</a></li> -->
              
            </ul>
          </div>

          <div class="col-lg-3 col-md-12 footer-contact text-center text-md-start">
            <h4>Contact Us</h4>
            <p>{{ $website->contact_alamat }}
              <!-- Jalan H. Said, Dsn. Melati<br>
              Ds. Tebas Sungai, Kec. Tebas<br>
              Kab. Sambas Kalimantan Barat<br>
              Kode Pos 79461 <br>
              Indonesia <br> -->
              <strong>Phone:</strong> (0562) 371-221 / (0562) 392-708<br>
              <strong>Email:</strong> smkn01tebas@gmail.com<br>
            </p>

          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>BKK SMKN 1 Tebas</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/flexstart-bootstrap-startup-template/ -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
  
  <!-- script vue.js -->
  <script src="{{ asset('js/app.js') }}"></script>

  <!-- Vendor JS Files -->
  <script src="{{ asset('assets_frontend/assets/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
  <script src="{{ asset('assets_frontend/assets/vendor/aos/aos.js') }}"></script>
  <script src="{{ asset('assets_frontend/assets/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ asset('assets_frontend/assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
  <script src="{{ asset('assets_frontend/assets/vendor/purecounter/purecounter.js') }}"></script>
  <script src="{{ asset('assets_frontend/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('assets_frontend/assets/vendor/glightbox/js/glightbox.min.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets_frontend/assets/js/main.js') }}"></script>
  
  <script src="{{ asset('/assets_backend/assets/plugins/jquery/dist/jquery.min.js') }}"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    
    <!-- <script type="text/javascript" src="{{ asset('assets_backend/vendor/datatables/datatables.min.js') }}"></script> -->

    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.3/datatables.min.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
    $('#example-ok').DataTable();
    } );
    </script>

</body>

</html>