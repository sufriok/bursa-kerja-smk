@extends('frontend.layouts.main')

@section('content')

<main id="main">
<div id="app">
    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <!-- <ol>
          <li><a href="/">Beranda</a></li>
          <li>Profil</li>
        </ol>
        <h5>Profil Alumni</h5> -->

      </div>
    </section><!-- End Breadcrumbs -->

    <section id="contact" class="contact">

    <div class="container" data-aos="fade-up">

    <div class="row">
        
        <div class="col-md-12">
            <div class="card border-info mb-3">
                <div class="card-header">
                
                </div>
                <div class="card-body text-info">
                <div class="justify-content-start">
                    <div class="text-center">
                        <img src="{{ asset('/profil/'.Auth::user()->profil) }}"  class="rounded-circle img-fluid" width="70" alt="">
                        <h5 class="pt-1 mb-0">{{ $post->user->name }}</h5> 
                        <p class="card-text"><small class="text-muted">{{ $post->created_at->diffForHumans()}}</small></p>
                    </div>
                    <hr>
                    <div class="text-center">
                    @if(isset($post->image))
                        <div><img src="{{ asset('postImage/'.$post->image) }}" class="rounded img-fluid" alt=""></div><br>
                        <div><p class="card-text">{{ $post->content }}</p></div>
                    @else
                        <div><p class="card-text">{{ $post->content }}</p></div>
                    @endif
                    </div>
                </div>
                    <!-- <h5 class="card-title">Info card title</h5>
                    <p class="card-text">{{ $post->content }}</p> -->
                </div>
                <div class="card-footer">
                    <small class="text-muted"><i class="bi bi-people-fill"></i> {{ $post->comments->count() }} Komentar</small>
                    <br>

                </div>
            </div>
        </div>
        <div class="col-md-12">
            <commentpost v-bind:post="{{ $post }}"></commentpost>
        </div>
        
    </div>

    </div>

    </section><!-- End Contact Section -->
</div>
</main><!-- End #main -->
@endsection