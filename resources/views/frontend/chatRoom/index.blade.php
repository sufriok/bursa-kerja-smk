@extends('frontend.layouts.main')

@section('content')
<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <!-- <ol>
          <li><a href="/">Beranda</a></li>
          <li>Profil</li>
        </ol>
        <h5>Profil Alumni</h5> -->

      </div>
    </section><!-- End Breadcrumbs -->
<!-- <br>
    @if(Session::has('success'))
        <div class="alert alert-info alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif
    @if(Session::has('update'))
        <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('update') }}</strong>
        </div>
    @endif
    @if(Session::has('delete'))
        <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ Session::get('delete') }}</strong>
        </div>
    @endif
<br> -->
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">

      <div class="container" data-aos="fade-up">

        <!-- <header class="section-header">
          <h2>Contact</h2>
          <p>Contact Us</p>
        </header> -->

        <div class="row gy-4">

          <div class="col-lg-4">

            <div class="row gy-4">
              <div class="col">
                <div class="info-box">
                  <center>
                  <img src="{{ asset('/profil/'.Auth::user()->profil) }}"
                            class="rounded-circle" width="140" />
                  
                  <h3>{{ auth()->user()->name }}</h3>
                  <p>NISN: {{ $graduate->nisn }}<br>
                  Angkatan: {{ $graduate->th_lulus }} <br>
                  @if($graduate->keterangan == "kuliah")
                    <p>Keterangan: Kuliah</p> 
                  @elseif($graduate->keterangan == "bekerja")
                    <p>Keterangan: Bekerja</p> 
                  @elseif($graduate->keterangan == "wirausaha")
                    <p>Keterangan: Wirausaha</p> 
                  @else
                    <p>Keterangan: Tidak ada</p> 
                  @endif
                  <!-- @if($graduate->keterangan == "kuliah")
                  <select name="forma" onchange="location = this.value;">
                    <option value="#">Kuliah</option>
                    <option value="{{ route('ket.bekerja', $graduate) }}">Bekerja</option>
                    <option value="{{ route('ket.wirausaha', $graduate) }}">Wirausaha</option>
                  </select>
                  @elseif($graduate->keterangan == "bekerja")
                  <select name="forma" onchange="location = this.value;">
                    <option value="#">Bekerja</option>
                    <option value="{{ route('ket.kuliah', $graduate) }}">Kuliah</option>
                    <option value="{{ route('ket.wirausaha', $graduate) }}">Wirausaha</option>
                  </select>
                  @elseif($graduate->keterangan == "wirausaha")
                  <select name="forma" onchange="location = this.value;">
                    <option value="#">Wirausaha</option>
                    <option value="{{ route('ket.kuliah', $graduate) }}">Kuliah</option>
                    <option value="{{ route('ket.bekerja', $graduate) }}">Bekerja</option>
                  </select>
                  @else
                  <select name="forma" onchange="location = this.value;">
                    <option value="">Tidak ada</option><hr>
                    <option value="{{ route('ket.kuliah', $graduate) }}">Kuliah</option>
                    <option value="{{ route('ket.bekerja', $graduate) }}">Bekerja</option>
                    <option value="{{ route('ket.wirausaha', $graduate) }}">Wirausaha</option>
                  </select>
                  @endif -->
                  </p>                
            
                  </center>
                </div>
              </div>
            </div>

          </div>

          <div class="col-lg-8">
            <form action="#" class="form-edit">
              <div class="row gy-4">

                <div class="col-md-12">
                  <img src="{{ asset('/assets_frontend\assets\img\imagePostingan.png') }}"
                            class="rounded img-fluid"/>
                </div>

                <div class="col-md-12">
                  <input type="text" name="name" class="form-control" placeholder="Posting Sesuatu"  data-bs-toggle="modal" data-bs-target="#exampleModal">
                </div>

                <!-- <div class="col-md-12 text-center">
                  <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Launch demo modal
                  </button>
                </div> -->

              </div>
            </form>

          </div>

        </div>
        
        <!-- <br>
        <div class="row">
          <div class="col-md-12">
            <div class="card border-info mb-3">
                <div class="card-header">
                </div>
                <div class="card-body text-info">
                  <div class="row">
                    <div class="col-lg-1 col-md-1 col-sm-1">
                      <a href="index.html">
                        <div class="align-items-center">
                          <img src="{{ asset('images/'.$website->header_logo) }}" width="60" alt="">
                          <p class="m-2"><strong>Budi</strong><small class="text-muted"> 07:30</small></p>
                        </div>
                      </a>
                    </div>
                    <div class="col-lg-11 col-md-11 col-sm-11">
                        <div class="m-2 search">
                            <div class="breadcrumbs-item breadcrumbs-form">
                              <form action="" method="GET">
                                <input type="text" name="cari" value="{{ old('cari') }}">
                                <button type="submit"><i class="bi bi-cursor-fill"></i></button>
                              </form>
                            </div>
                        </div>
                    </div>
                  </div>
                    
                </div>
                <div class="card-footer">
                  
                  <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Launch demo modal
                  </button>
                </div>
            </div>
          </div>
        </div> -->
        <br><hr><br>

        <div class="row">
          @foreach($posts as $post)
          
            <div class="col-md-2"><br><br>
              <div class="text-center">
                  <img src="{{ asset('/profil/'.$post->user->profil) }}" class="rounded-circle img-fluid" width="70" alt="">
                  <h5 class="pt-1 mb-0">{{ $post->user->name }}</h5> 
                  <p class="card-text"><small class="text-muted">{{ $post->created_at->diffForHumans()}}</small></p>
              </div>
            </div>
            <div class="col-md-10">
              <a href="{{ route('chatroom.show', $post->id) }}">
                <div class="card border-info mb-3">
                    <!-- <div class="card-header">
                    <img src="{{ asset('images/logobkk.png') }}"  class="rounded-circle" width="50" alt="">  
                    {{ $post->user->name }}
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                    </div> -->
                    <div class="card-footer text-info"><br>
                      <div class="justify-content-start"> 
                        @if(isset($post->image))
                          <div><img src="{{ asset('postImage/'.$post->image) }}" class="rounded img-fluid" width="450" alt=""></div><br>
                          <div><p class="card-text">{{ $post->content }}</p></div>
                        @else
                          <div><p class="card-text">{{ $post->content }}</p></div>
                        @endif
                      </div>
                    </div>
                    <div class="card-body">
                        <small class="text-muted"><i class="bi bi-people-fill"></i> {{ $post->comments->count() }} Komentar</small>
                        <br>
                        @if(isset($post->comments->last()->content))
                          <a href="{{ route('chatroom.show', $post->id) }}">
                            <div class="logo d-flex align-items-center">
                            <img src="{{ asset('/profil/'.$post->user->profil) }}" class="rounded-circle" width="25" alt="">
                            <h6 class="m-2">{{ $post->user->name }}</h6><small class="text-muted"> {{ $post->comments->last()->created_at->diffForHumans() }}</small>
                            </div>
                            <h6 class="m-2"><strong>{{ $post->comments->last()->content }}</strong></h6>
                          </a>
                        @else
                        
                        @endif
                        
                        <!-- <hr> -->

                        <!-- <div class="input-group mb-3">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon1">Send</button>
                            <input type="text" class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
                        </div> -->

                    </div>
                </div>
              </a>
            </div>
          
          @endforeach
        </div>

      </div>

    </section><!-- End Contact Section -->
  </main><!-- End #main -->
@endsection

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Posting</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form method="post" action="{{ route('chatroom.store') }}" enctype="multipart/form-data">
      @csrf
        <div class="modal-body">
          
            <div class="mb-3">
              <label for="recipient-name" class="col-form-label">Recipient:</label>
              <input type="file" name="image" class="form-control" id="recipient-name">
            </div>
            <div class="mb-3">
              <label for="message-text" class="col-form-label">Message:</label>
              <textarea class="form-control" name="content"  rows="10" id="message-text"></textarea>
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Post</button>
        </div>
      </form>
    </div>
  </div>
</div>