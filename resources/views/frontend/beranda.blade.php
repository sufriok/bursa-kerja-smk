@extends('frontend.layouts.main')

@section('content')

<!-- ======= Hero Section ======= -->
<section id="hero" class="hero d-flex align-items-center">

<div class="container">
  <div class="row">
    <div class="col-lg-6 d-flex flex-column justify-content-center">
      <h1 data-aos="fade-up">{{ $website->header_kalimat1 }}</h1>
      <h2 data-aos="fade-up" data-aos-delay="400">{{ $website->header_kalimat1 }}</h2>
      <div data-aos="fade-up" data-aos-delay="600">
        <div class="text-center text-lg-start">
          <a href="#about" class="btn-get-started scrollto d-inline-flex align-items-center justify-content-center align-self-center">
            <span>Get Started</span>
            <i class="bi bi-arrow-right"></i>
          </a>
        </div>
      </div>
    </div>
    <div class="col-lg-6 hero-img" data-aos="zoom-out" data-aos-delay="200"> 
      <img src="{{ asset('images/progress-work2.png') }}" class="img-fluid" alt="">
    </div>
  </div>
</div>

</section><!-- End Hero -->


<main id="main">
<!-- ======= About Section ======= -->
<section id="about" class="about">

  <div class="container" data-aos="fade-up">
    <div class="row gx-0">

      <div class="col-lg-6 d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="200">
        <div class="content">
          <h3>Who We Are</h3>
          <h2>{{ $website->about_title }}</h2>
          <p>
          {{ $website->about_deskripsi }}
          </p>
          <div class="text-center text-lg-start">
            <a href="#" class="btn-read-more d-inline-flex align-items-center justify-content-center align-self-center">
              <span>Read More</span>
              <i class="bi bi-arrow-right"></i>
            </a>
          </div>
        </div>
      </div>

      <div class="col-lg-6 d-flex align-items-center" data-aos="zoom-out" data-aos-delay="200">
        <img src="{{ asset('images/'.$website->about_foto) }}" class="img-fluid" alt="">
      </div>

    </div>
  </div>

</section><!-- End About Section -->

<!-- ======= Values Section ======= -->
<!-- <section id="values" class="values">

  <div class="container" data-aos="fade-up">

    <header class="section-header">
      <h2>Our Values</h2>
      <p>Odit est perspiciatis laborum et dicta</p>
    </header>

    <div class="row">

      <div class="col-lg-4">
        <div class="box" data-aos="fade-up" data-aos-delay="200">
          <img src="{{ asset('assets_frontend/assets/img/values-1.png') }}" class="img-fluid" alt="">
          <h3>Ad cupiditate sed est odio</h3>
          <p>Eum ad dolor et. Autem aut fugiat debitis voluptatem consequuntur sit. Et veritatis id.</p>
        </div>
      </div>

      <div class="col-lg-4 mt-4 mt-lg-0">
        <div class="box" data-aos="fade-up" data-aos-delay="400">
          <img src="{{ asset('assets_frontend/assets/img/values-2.png') }}" class="img-fluid" alt="">
          <h3>Voluptatem voluptatum alias</h3>
          <p>Repudiandae amet nihil natus in distinctio suscipit id. Doloremque ducimus ea sit non.</p>
        </div>
      </div>

      <div class="col-lg-4 mt-4 mt-lg-0">
        <div class="box" data-aos="fade-up" data-aos-delay="600">
          <img src="{{ asset('assets_frontend/assets/img/values-3.png') }}" class="img-fluid" alt="">
          <h3>Fugit cupiditate alias nobis.</h3>
          <p>Quam rem vitae est autem molestias explicabo debitis sint. Vero aliquid quidem commodi.</p>
        </div>
      </div>

    </div>

  </div>

</section> -->
<!-- End Values Section -->

<!-- ======= Counts Section ======= -->
<section id="counts" class="counts">
  <div class="container" data-aos="fade-up">

    <div class="row gy-4">

      <!-- <div class="col-lg-3 col-md-6">
        <div class="count-box">
          <i class="bi bi-people"></i>
          <div>
            <span data-purecounter-start="0" data-purecounter-end="{{ $jmlhgraduate }}" data-purecounter-duration="1" class="purecounter"></span>
            <p>Jumlah Alumni</p>
          </div>
        </div>
      </div> -->

      <!-- <div class="col-lg-4 col-md-6">
        <div class="count-box">
          <i class="bi bi-journal-richtext" style="color: #ee6c20;"></i>
          <div>
            <span data-purecounter-start="0" data-purecounter-end="{{ $jmlhloker }}" data-purecounter-duration="1" class="purecounter"></span>
            <p>Jumlah Loker</p>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-6">
        <div class="count-box">
          <i class="bi bi-headset" style="color: #15be56;"></i>
          <div>
            <span data-purecounter-start="0" data-purecounter-end="1463" data-purecounter-duration="1" class="purecounter"></span>
            <p>kategori loker</p>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-6">
        <div class="count-box">
          <i class="bi bi-emoji-smile" style="color: #bb0852;"></i>
          <div>
            <span data-purecounter-start="0" data-purecounter-end="{{ $jmlhmou }}" data-purecounter-duration="1" class="purecounter"></span>
            <p>Jumlah MOU</p>
          </div>
        </div>
      </div> -->

    </div>

  </div>
</section><!-- End Counts Section -->

<!-- ======= Recent Blog Posts Section ======= -->
<section id="recent-blog-posts" class="recent-blog-posts">

  <div class="container" data-aos="fade-up">

    <header class="section-header">
      <h2>Info Loker</h2>
      <p>Info Lowongan Kerja</p>
    </header>

    <div class="row">

      @foreach ($lokers as $loker)
      <div class="col-lg-4 pb-3">
        <div class="post-box">
          <div class="post-img"><img src="{{ asset('/brosurKerja/'.$loker->brosur) }}" class="img-fluid" alt=""></div>
          <span class="post-date">{{ $loker->tgl_awal}} - {{ $loker->tgl_akhir}}</span>
          <h3 class="post-title">{{ $loker->title}}</h3>
          <a href="{{ route('show.loker', $loker->id) }}" class="readmore stretched-link mt-auto"><span>Read More</span><i class="bi bi-arrow-right"></i></a>
        </div>
      </div>
      @endforeach

      <!-- <div class="col-lg-4">
        <div class="post-box">
          <div class="post-img"><img src="{{ asset('assets_frontend/assets/img/blog/blog-2.jpg') }}" class="img-fluid" alt=""></div>
          <span class="post-date">Fri, August 28</span>
          <h3 class="post-title">Et repellendus molestiae qui est sed omnis voluptates magnam</h3>
          <a href="blog-singe.html" class="readmore stretched-link mt-auto"><span>Read More</span><i class="bi bi-arrow-right"></i></a>
        </div>
      </div>

      <div class="col-lg-4">
        <div class="post-box">
          <div class="post-img"><img src="{{ asset('assets_frontend/assets/img/blog/blog-3.jpg') }}" class="img-fluid" alt=""></div>
          <span class="post-date">Mon, July 11</span>
          <h3 class="post-title">Quia assumenda est et veritatis aut quae</h3>
          <a href="blog-singe.html" class="readmore stretched-link mt-auto"><span>Read More</span><i class="bi bi-arrow-right"></i></a>
        </div>
      </div> -->

    </div>

  </div>
  <div class="contact mt-4">
    <div class="info-box">
      <div class="container">
        <div class="row">
          <div class="col-8 text-center">
            <h2 style="color:#012970">INFORMASI LOWONGAN KERJA..?</h2>
          </div>
          <div class="col-4 text-center">
          <a href="{{ route('show.all') }}" class="btn btn-primary py-2">
            <span>KLIK DISINI</span>
          </a>
          </div>
        </div>
      </div>
    </div>
  </div>

</section><!-- End Recent Blog Posts Section -->

<!-- ======= Contact Section ======= -->
<!-- <section id="contact" class="contact">

  <div class="container" data-aos="fade-up">

    <header class="section-header">
      <h2>Kontak</h2>
      <p>Kontak Kami</p>
    </header>
    
    <div class="row gy-4">

      <div class="col-lg-6">

        <div class="row gy-4">
          <div class="col-md-6">
            <div class="info-box">
              <i class="bi bi-geo-alt"></i>
              <h3>Address</h3> -->
              <!-- <p>Jalan H. Said, Dsn. Melati,<br>Ds. Tebas Sungai, Kec. Tebas,<br>Kode Pos 79461</p> -->
              <!-- {{ $website->contact_alamat }}
            </div>
          </div>
          <div class="col-md-6">
            <div class="info-box">
              <i class="bi bi-telephone"></i>
              <h3>Call Us</h3>
              <p>{{ $website->contact_phone }}<br>(0562) 392-708</p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="info-box">
              <i class="bi bi-envelope"></i>
              <h3>Email Us</h3> -->
              <!-- <p>info@example.com<br>contact@example.com</p> -->
              <!-- <p>{{ $website->contact_email }}</p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="info-box">
              <i class="bi bi-clock"></i>
              <h3>Open Hours</h3>
              <p>Senin - Sabtu<br>07:00 - 12:00</p>
            </div>
          </div>
        </div>

      </div>

      <div class="col-lg-6">
        <form action="{{ route('kirim.message') }}" method="post" class="form-edit">
        @csrf
          <div class="row gy-4">
          
            @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <div class="d-flex align-items-center">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                <div>
                <strong>Success! </strong>{{ Session::get('success') }}
                </div>
                
              </div>
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif

            <div class="col-md-6">
              <input type="text" name="name" class="form-control" placeholder="Your Name" required>
            </div>

            <div class="col-md-6 ">
              <input type="email" class="form-control" name="email" placeholder="Your Email" required>
            </div>

            <div class="col-md-12">
              <input type="text" class="form-control" name="subject" placeholder="Subject" required>
            </div>

            <div class="col-md-12">
              <textarea class="form-control" name="pesan" rows="6" placeholder="Message" required></textarea>
            </div>

            <div class="col-md-12 text-center">
              <button type="submit">Send Message</button>
            </div> 

          </div>
        </form>

      </div>

    </div>

  </div>

</section> -->
<!-- End Contact Section -->

<!-- ======= Clients Section ======= -->
<section id="clients" class="clients">

  <div class="container" data-aos="fade-up">

    <header class="section-header">
      <h2>M.O.U</h2>
      <p>Memorandum Of Understanding</p>
    </header>

    <div class="clients-slider swiper-container">
      <div class="swiper-wrapper align-items-center">
        @foreach($mous as $mou)
        <div class="swiper-slide"><img src="{{ asset('/logoMou/'.$mou->logo) }}" class="img-fluid" alt=""></div>
        @endforeach
        <!-- <div class="swiper-slide"><img src="{{ asset('assets_frontend/assets/img/clients/client-2.png') }}" class="img-fluid" alt=""></div>
        <div class="swiper-slide"><img src="{{ asset('assets_frontend/assets/img/clients/client-3.png') }}" class="img-fluid" alt=""></div>
        <div class="swiper-slide"><img src="{{ asset('assets_frontend/assets/img/clients/client-4.png') }}" class="img-fluid" alt=""></div>
        <div class="swiper-slide"><img src="{{ asset('assets_frontend/assets/img/clients/client-5.png') }}" class="img-fluid" alt=""></div>
        <div class="swiper-slide"><img src="{{ asset('assets_frontend/assets/img/clients/client-6.png') }}" class="img-fluid" alt=""></div>
        <div class="swiper-slide"><img src="{{ asset('assets_frontend/assets/img/clients/client-7.png') }}" class="img-fluid" alt=""></div>
        <div class="swiper-slide"><img src="{{ asset('assets_frontend/assets/img/clients/client-8.png') }}" class="img-fluid" alt=""></div> -->
      </div>
      <div class="swiper-pagination"></div>
    </div>
  </div>

</section><!-- End Clients Section -->

</main><!-- End #main -->
@endsection

<!-- <div class="row mt-4">

    <div class="col">
      <div class="post-box bg-secondary">
        <h3 class="post-date text-white">Eum ad dolor et. Autem aut fugiat debitis voluptatem consequuntur sit</h3>
        <a href="blog-singe.html" class="readmore stretched-link mt-auto"><span>Read More</span><i class="bi bi-arrow-right"></i></a>
      </div>
    </div>

  </div> -->