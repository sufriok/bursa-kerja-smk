@extends('frontend.layouts.main')

@section('content')

<main id="main">

  <!-- ======= Breadcrumbs ======= -->
  <section class="breadcrumbs">
    <div class="container">
      <center class="blog">
        <div class="search">
            <h3 class="breadcrumbs-title">Cari Lowongan Kerja</h3>
            <div class="breadcrumbs-item breadcrumbs-form">
              <form action="{{ route('cari.loker') }}" method="GET">
                <input type="text" name="cari" value="{{ old('cari') }}">
                <button type="submit"><i class="bi bi-search"></i></button>
              </form>
            </div>
        </div>
      </center>
      <!-- <footer id="footer" class="footer">
        <div class="footer-newsletter">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-12 text-center">
                <h4>Our Newsletter</h4>
                <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
              </div>
              <div class="col-lg-6">
                <form action="" method="post">
                  <input type="email" name="email"><input type="submit" value="Subscribe">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div> -->
    </div> <!-- container -->
    
  </section><!-- End Breadcrumbs -->

  <!-- ======= Blog Section ======= -->
  <section id="blog" class="blog">
    <div class="container" data-aos="fade-up">

      <div class="row">
        
        <div class="col-lg-8 entries">
          @foreach($lokers as $loker)
          <article class="entry">

            <div class="entry-img">
              <img src="{{ asset('/brosurKerja/'.$loker->brosur) }}" alt="" class="img-fluid">
            </div>

            <h2 class="entry-title">
              <a href="{{ route('show.loker', $loker->id) }}">{{ $loker->title}}</a>
            </h2>

            <div class="entry-meta">
              <ul>
                <li class="d-flex align-items-center"><i class="bi bi-date"></i> <a href="blog-single.html">{{ $loker->tgl_awal}} - {{ $loker->tgl_akhir}}</a></li>
              </ul>
            </div>

            <div class="entry-content">
              <p>{{ $loker->deskripsi}}</p>
              <div class="read-more">
                <a href="{{ route('show.loker', $loker->id) }}">Read More</a>
              </div>
            </div>

          </article><!-- End blog entry -->
          @endforeach
          <div class="blog-pagination">
            <ul class="justify-content-center">
              {{ $lokers->links() }}
            </ul>
          </div>
          <br>
        </div><!-- End blog entries list -->
        

        <div class="col-lg-4">

          <div class="sidebar">

            <h3 class="sidebar-title">Search</h3>
            <div class="sidebar-item search-form">
              <form action="{{ route('cari.loker') }}" method="GET">
                <input type="text" name="cari" value="{{ old('cari') }}">
                <button type="submit"><i class="bi bi-search"></i></button>
              </form>
            </div><!-- End sidebar search formn-->

            <h3 class="sidebar-title">Categories</h3>
            <div class="sidebar-item categories">
              <ul>
              @foreach( $categories as $category )
                <li><a href="{{ route('category.loker', $category) }}">{{ $category->kategori }} <span>({{ $category->loker->count() }})</span></a></li>
              @endforeach
              </ul>
            </div><!-- End sidebar categories-->

            <h3 class="sidebar-title">Recent Posts</h3>
            <div class="sidebar-item recent-posts">
              @foreach ($newlokers as $newloker)
              <div class="post-item clearfix">
                <img src="{{ asset('/brosurKerja/'.$newloker->brosur) }}" alt="">
                <h4><a href="{{ route('show.loker', $newloker->id) }}">{{ $newloker->title}}</a></h4>
                <time>{{ $newloker->tgl_awal}} - {{ $newloker->tgl_akhir}}</time>
              </div>
              @endforeach

              <!-- <div class="post-item clearfix">
                <img src="{{ asset('assets_frontend/assets/img/blog/blog-recent-2.jpg') }}" alt="">
                <h4><a href="blog-single.html">Quidem autem et impedit</a></h4>
                <time datetime="2020-01-01">Jan 1, 2020</time>
              </div>

              <div class="post-item clearfix">
                <img src="{{ asset('assets_frontend/assets/img/blog/blog-recent-3.jpg') }}" alt="">
                <h4><a href="blog-single.html">Id quia et et ut maxime similique occaecati ut</a></h4>
                <time datetime="2020-01-01">Jan 1, 2020</time>
              </div>

              <div class="post-item clearfix">
                <img src="{{ asset('assets_frontend/assets/img/blog/blog-recent-4.jpg') }}" alt="">
                <h4><a href="blog-single.html">Laborum corporis quo dara net para</a></h4>
                <time datetime="2020-01-01">Jan 1, 2020</time>
              </div>

              <div class="post-item clearfix">
                <img src="{{ asset('assets_frontend/assets/img/blog/blog-recent-5.jpg') }}" alt="">
                <h4><a href="blog-single.html">Et dolores corrupti quae illo quod dolor</a></h4>
                <time datetime="2020-01-01">Jan 1, 2020</time>
              </div> -->

            </div><!-- End sidebar recent posts-->

            <h3 class="sidebar-title">Tags</h3>
            <div class="sidebar-item tags">
              <ul>
                <li><a href="#">App</a></li>
                <li><a href="#">IT</a></li>
                <li><a href="#">Business</a></li>
                <li><a href="#">Mac</a></li>
                <li><a href="#">Design</a></li>
                <li><a href="#">Office</a></li>
                <li><a href="#">Creative</a></li>
                <li><a href="#">Studio</a></li>
                <li><a href="#">Smart</a></li>
                <li><a href="#">Tips</a></li>
                <li><a href="#">Marketing</a></li>
              </ul>
            </div>
            <!-- End sidebar tags-->

          </div><!-- End sidebar -->

        </div><!-- End blog sidebar -->

      </div>

    </div>
  </section><!-- End Blog Section -->

</main><!-- End #main -->

@endsection